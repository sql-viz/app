// @flow
import { Digraph } from '../../models';
import { ensureHashValue } from '../../utils/hash';

export class CycleRemoval {
  digraph: Digraph;
  constructor(digraph: Digraph) {
    this.digraph = digraph;
  }
  solve() {
    const { alphabet } = this;
    const neighborHash = {};
    const swappedEdges = {};
    if (this.digraph.edges.length === 0) {
      return { digraph: this.digraph, swappedEdges };
    }
    this.digraph.edges.forEach(({ source, sink }) => {
      ensureHashValue(neighborHash, source, {});
      ensureHashValue(neighborHash, sink, {});
      if (alphabet[source] < alphabet[sink]) {
        neighborHash[source][sink] = true;
      } else {
        ensureHashValue(swappedEdges, sink, {});
        swappedEdges[sink][source] = true;
        neighborHash[sink][source] = true;
      }
    });
    const nodes = this.digraph.nodeList.map((node) => ({ ...node, neighbors: Object.keys(neighborHash[node.id]) || [] }));

    return { digraph: new Digraph(nodes), swappedEdges };
  }

  get alphabet() {
    throw new Error('not implemented');
  }
}
