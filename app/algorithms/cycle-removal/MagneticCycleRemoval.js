
import { CycleRemoval } from './CycleRemoval';

export class MagneticCycleRemoval extends CycleRemoval {
  get polarityHash() {
    const polarityHash = {};

    const { sinkHash, sourceHash, nodeList } = this.digraph;
    nodeList.forEach(({ id }) => {
      const sinks = Object.values(sinkHash[id]).length;
      const sources = Object.values(sourceHash[id]).length;
      polarityHash[id] = sinks - sources;
    });
    return polarityHash;
  }

  get alphabet() {
    const { polarityHash } = this;
    const alphabet = {};
    this.digraph.nodeList.map(node => node.id)
      .sort((id1, id2) => polarityHash[id2] - polarityHash[id1])
      .forEach((id, index) => {
        alphabet[id] = index;
      });
    return alphabet;
  }
}
