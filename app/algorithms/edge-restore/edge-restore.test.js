
import { restoreEdges } from '.';
import { Digraph } from '../../models';

describe('restore edges', () => {
  test('simple', () => {
    const digraph = new Digraph([
      { id: 'A', neighbors: ['B'] },
      { id: 'B' },
    ]);

    expect(restoreEdges(digraph, { A: { B: true } }).nodeList).toEqual(expect.arrayContaining([
      expect.objectContaining({ id: 'A', neighbors: [] }),
      expect.objectContaining({ id: 'B', neighbors: ['A'] }),
    ]));
  });

  test('with single path', () => {
    const digraph = new Digraph([
      { id: 'A', neighbors: ['v1'] },
      { id: 'v1', neighbors: ['B'], virtual: true },
      { id: 'B', neighbors: [] },
    ]);

    expect(restoreEdges(digraph, { A: { B: true } }).nodeList).toEqual(expect.arrayContaining([
      expect.objectContaining({ id: 'A', neighbors: [] }),
      expect.objectContaining({ id: 'v1', neighbors: ['A'] }),
      expect.objectContaining({ id: 'B', neighbors: ['v1'] }),
    ]));
  });
});
