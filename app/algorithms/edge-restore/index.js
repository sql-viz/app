
import { Digraph } from '../../models';
import { ensureHashValue } from '../../utils/hash';

export function restoreEdges(digraph, swappedEdges) {
  const { edges, nodeList, sinkHash } = digraph;
  const edgesToSwap = {};

  function descend(u, v) {
    if (u === v) return true;
    const ns = Object.keys(sinkHash[u]);
    if (ns.length === 0) return false;
    return ns.some((n) => {
      const shouldFlip = descend(n, v);
      if (shouldFlip) {
        ensureHashValue(edgesToSwap, u, {});
        edgesToSwap[u][n] = true;
      }
      return shouldFlip;
    });
  }

  Object.keys(swappedEdges).forEach((u) => {
    Object.keys(swappedEdges[u]).forEach((v) => {
      descend(u, v);
    });
  });

  const neighbors = {};
  nodeList.forEach((node) => {
    neighbors[node.id] = {};
  });
  edges.forEach((e) => {
    const { source: u, sink: v } = e;
    if (u in edgesToSwap && v in edgesToSwap[u]) {
      neighbors[v][u] = true;
    } else {
      neighbors[u][v] = true;
    }
  });

  return new Digraph(nodeList.map(node => ({
    ...node,
    neighbors: Object.keys(neighbors[node.id]),
  })));
}
