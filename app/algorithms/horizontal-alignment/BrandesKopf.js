
import { Digraph } from '../../models';
import { createDictionary } from '../../utils';

export class BrandesKopf {
  constructor(digraph, layering, ordering, config = { delta: 50, nodeHeights: undefined }) {
    this.digraph = digraph;
    this.layering = layering;
    this.ordering = ordering;
    this.layerOrdering = this.createLayerOrdering();
    this.config = config;
  }

  nodeHeight(u) {
    const { config: { nodeHeights } } = this;
    return nodeHeights[u];
  }

  delta(u, v) {
    const { delta } = this.config;
    return this.nodeHeight(u) + delta;
  }

  createLayerOrdering(ordering = this.ordering) {
    const { layering } = this;
    const layerOrdering = [];
    Object.keys(layering).forEach((node) => {
      const l = layering[node];
      const o = ordering[l][node];
      if (!(l in layerOrdering)) {
        layerOrdering[l] = [];
      }
      layerOrdering[l][o] = node;
    });
    return layerOrdering;
  }

  markTypeXConflicts(markingPredicate) {
    const { layerOrdering, digraph, ordering } = this;
    const { nodes, sinkHash: neighbors } = digraph;
    const layerCount = layerOrdering.length;
    const marked = [];
    for (let i = 1; i <= layerCount - 3; i += 1) {
      let [k0, l] = [-1, 0];
      const currentLayer = layerOrdering[i];
      const nextLayer = layerOrdering[i + 1];
      for (let l1 = 0; l1 < nextLayer.length; l1 += 1) {
        const node = nextLayer[l1];
        const firstVirtualNeighbor = Object.keys(neighbors[node])
          .filter(n => nodes[n].virtual)
          .sort((n1, n2) => ordering[i][n1] - ordering[i][n2])[0];
        const isIncident = (firstVirtualNeighbor && nodes[node].virtual);
        if (isIncident || l1 === nextLayer.length - 1) {
          const k1 = isIncident ? ordering[i][firstVirtualNeighbor] : currentLayer.length;
          while (l < l1) {
            const lNeighbors = Object.keys(neighbors[nextLayer[l]]);
            for (const vK of lNeighbors) {
              const k = ordering[i][vK];
              const predicate = !markingPredicate || markingPredicate(nodes[vK], nodes[nextLayer[l]]);
              if (predicate && (k < k0 || k > k1)) {
                marked.push([vK, nextLayer[l]]);
              }
            }
            l += 1;
          }
          k0 = k1;
        }
      }
    }
    return marked;
  }

  markType1Conflicts() {
    return this.markTypeXConflicts();
  }

  markType2Conflicts() {
    return this.markTypeXConflicts((n1, n2) => n1.virtual && n2.virtual);
  }

  getNodePosition(ordering, v) {
    const { layering } = this;
    return ordering[layering[v]][v];
  }

  getNodePredecessor(ordering, layerOrdering, v) {
    const { layering } = this;
    return layerOrdering[layering[v]][this.getNodePosition(ordering, v) - 1];
  }

  flipOrdering(ordering) {
    const layerLengths = {};
    Object.keys(ordering).forEach((layer) => {
      layerLengths[layer] = Object.keys(ordering[layer]).length;
    });
    const flippedOrdering = {};
    Object.keys(ordering).forEach((layer) => {
      flippedOrdering[layer] = {};
      Object.keys(ordering[layer]).forEach((node) => {
        flippedOrdering[layer][node] = layerLengths[layer] - ordering[layer][node] - 1;
      });
    });
    return flippedOrdering;
  }

  flipX(x) {
    const max = Math.max(...Object.values(x));
    const flippedX = {};
    Object.keys(x).forEach((node) => {
      flippedX[node] = max - x[node];
    });
    return flippedX;
  }

  normalizeCoordinates(xs) {
    const widths = xs.map(x => Math.max(...Object.values(x)));
    const minWidth = Math.min(...widths);
    return xs.map((x, idx) => {
      const xNormalized = {};
      Object.keys(x).forEach((node) => {
        xNormalized[node] = (minWidth / widths[idx]) * x[node];
      });
      return xNormalized;
    });
  }

  getAverageMedian(xs) {
    const [x0] = xs;
    const medians = {};
    Object.keys(x0).forEach((node) => {
      const values = [];
      xs.forEach((x) => {
        values.push(x[node]);
      });
      values.sort((a, b) => a - b);
      medians[node] = (values[1] + values[2]) / 2.0;
    });
    return medians;
  }

  verticalAlignment(marked, ordering, layerOrdering, neighbors, down) {
    const { digraph } = this;

    const [root, align] = [createDictionary(Object.keys(digraph.nodes)), createDictionary(Object.keys(digraph.nodes))];
    const s = down ? 1 : layerOrdering.length - 2;
    const e = down ? layerOrdering.length : -1;
    const inc = down ? 1 : -1;
    for (let i = s; i !== e; i += inc) {
      let r = -1;
      const layer = layerOrdering[i];
      for (let k = 0; k < layer.length; k += 1) {
        const v = layer[k];
        const orderedNeighbors = Object.keys(neighbors[v]).sort((n1, n2) => ordering[i][n1] - ordering[i][n2]);
        const d = orderedNeighbors.length;
        if (d > 0) {
          [Math.floor, Math.ceil].map(fn => fn((d + 1) / 2) - 1).forEach((m) => {
            const u = orderedNeighbors[m];
            const neighborPos = ordering[i - inc][u];
            if (align[v] === v && !marked[JSON.stringify([u, v])] && r < neighborPos) {
              align[u] = v;
              root[v] = root[u];
              align[v] = root[v];
              r = neighborPos;
            }
          });
        }
      }
    }

    return { root, align };
  }

  horizontalCompaction(align, root, ordering, layerOrdering) {
    const { digraph } = this;
    const delta = this.delta.bind(this);
    const nodes = Object.keys(digraph.nodes);
    const [sink, shift, x] = [
      createDictionary(nodes),
      createDictionary(nodes, undefined, () => Number.POSITIVE_INFINITY),
      createDictionary(nodes, undefined, () => undefined),
    ];

    const getNodePosition = v => this.getNodePosition(ordering, v);
    const getNodePredecessor = v => this.getNodePredecessor(ordering, layerOrdering, v);

    function placeBlock(v) {
      if (x[v] === undefined) {
        x[v] = 0;
        let w = v;
        do {
          if (getNodePosition(w) > 0) {
            const predecessor = getNodePredecessor(w);
            const u = root[predecessor];
            placeBlock(u);
            if (sink[v] === v) sink[v] = sink[u];
            if (sink[v] !== sink[u]) {
              shift[sink[u]] = Math.min(shift[sink[u]], x[v] - x[u] - delta(predecessor, v));
            } else {
              x[v] = Math.max(x[v], x[u] + delta(predecessor, v));
            }
          }
          w = align[w];
        } while (w !== v);
      }
    }

    nodes.filter(v => root[v] === v).forEach(v => placeBlock(v));

    nodes.forEach((v) => {
      x[v] = x[root[v]];
      if (shift[sink[root[v]]] < Number.POSITIVE_INFINITY) {
        x[v] += shift[sink[root[v]]];
      }
    });

    return x;
  }

  solve() {
    const marked = createDictionary(this.markType1Conflicts(), edge => JSON.stringify(edge), () => true);
    const xs = [];
    const { ordering, digraph, layering } = this;
    const { sinkHash, sourceHash } = digraph;
    [true, false].forEach((right) => {
      const directedOrdering = right ? ordering : this.flipOrdering(ordering);
      const directedLayerOrdering = this.createLayerOrdering(directedOrdering);
      [true, false].forEach((down) => {
        const neighbors = down ? sinkHash : sourceHash;
        const { align, root } = this.verticalAlignment(marked, directedOrdering, directedLayerOrdering, neighbors, down);
        const x = this.horizontalCompaction(align, root, directedOrdering, directedLayerOrdering);
        const xCorrected = right ? x : this.flipX(x, layering);
        xs.push(xCorrected);
      });
    });

    const medians = this.getAverageMedian(xs);

    return new Digraph(
      Object.values(digraph.nodes),
      {
        y: v => medians[v.id],
        x: v => layering[v.id] * 200,
      }
    );
  }
}
