
import { Digraph } from '../../models';
import { createDictionary } from '../../utils';

export function islandify(digraph) {
  const { sinkHash, sourceHash, nodes, nodeList } = digraph;
  const unprocessed = createDictionary(nodeList, node => node.id, () => true);
  const islands = [];
  let i = 0;

  function visit(node) {
    delete unprocessed[node];
    islands[i].push(nodes[node]);
    Object.keys(sinkHash[node])
      .concat(Object.keys(sourceHash[node]))
      .filter(n => n in unprocessed)
      .forEach(visit);
  }

  while (Object.keys(unprocessed).length > 0) {
    const node = Object.keys(unprocessed)[0];
    islands[i] = [];
    visit(node);
    i += 1;
  }

  return islands.map(islandNodes => new Digraph(islandNodes));
}
