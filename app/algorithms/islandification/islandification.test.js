
import { Digraph } from '../../models';
import { islandify } from '.';

describe('islandification', () => {
  test('islandification single', () => {
    const digraph = new Digraph([
      { id: 'A', neighbors: ['B'] },
      { id: 'B', neighbors: [] },
    ]);

    const islands = islandify(digraph);
    expect(islands).toHaveLength(1);
    expect(islands.map(island => island.nodeList.map(node => node.id))).toEqual(expect.arrayContaining([
      expect.arrayContaining(['A', 'B']),
    ]));
  });

  test('islandification simple', () => {
    const digraph = new Digraph([
      { id: 'A', neighbors: ['B'] },
      { id: 'B', neighbors: [] },
      { id: 'C', neighbors: [] },
    ]);

    const islands = islandify(digraph);
    expect(islands).toHaveLength(2);
    expect(islands.map(island => island.nodeList.map(node => node.id))).toEqual(expect.arrayContaining([
      expect.arrayContaining(['A', 'B']),
      expect.arrayContaining(['C']),
    ]));
  });

  test('islandification elaborate', () => {
    const digraph = new Digraph([
      { id: 'A', neighbors: ['B'] },
      { id: 'B', neighbors: ['C'] },
      { id: 'C', neighbors: ['A'] },
      { id: 'E', neighbors: ['F'] },
      { id: 'F', neighbors: [] },
      { id: 'G', neighbors: [] },
      { id: 'H', neighbors: ['G'] },
    ]);

    const islands = islandify(digraph);
    expect(islands).toHaveLength(3);
    expect(islands.map(island => island.nodeList.map(node => node.id))).toEqual(expect.arrayContaining([
      expect.arrayContaining(['A', 'B', 'C']),
      expect.arrayContaining(['E', 'F']),
      expect.arrayContaining(['G', 'H']),
    ]));
  });
});
