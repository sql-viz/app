
import { Layering } from './layering';

export class BFSLayering extends Layering {
  get layering() {
    const { startingNode, digraph } = this;
    const { sourceHash, sinkHash } = digraph;
    const queue = [{ id: startingNode.id, layer: 0 }];
    const layering = {};
    while (queue.length > 0) {
      const { id, layer } = queue.shift();
      if (id in layering) {
        continue; //  eslint-disable-line
      }
      const belows = Object.keys(sourceHash[id]);
      belows.forEach((neighborId) => {
        queue.push({ id: neighborId, layer: layer + 1 });
      });
      const aboves = Object.keys(sinkHash[id]);
      aboves.forEach((neighborId) => {
        queue.push({ id: neighborId, layer: layer - 1 });
      });
      layering[id] = layer;
    }
    return layering;
  }

  get startingNode() {
    const { sourceHash, nodeList } = this.digraph;
    const sourceCountHash = {};
    Object.keys(sourceHash).forEach((id) => {
      sourceCountHash[id] = Object.values(sourceHash[id]).length;
    });
    return nodeList.reduce((top, node) => (sourceCountHash[top.id] >= sourceCountHash[node.id] ? top : node));
  }
}
