
// @flow
import shortid from 'shortid';
import { Digraph } from '../../models';

export type LayeringHash = { [key: string]: number };

export class Layering {
  constructor(digraph: Digraph) {
    this.digraph = digraph;
  }

  get layering(): LayeringHash {
    throw new Error('not implemeted');
  }

  solve(): { digraph: Digraph, layering: LayeringHash } {
    const { digraph, layering } = this;
    const { sinkHash } = digraph;
    const nodes = {};
    const newLayering = { ...layering };
    digraph.nodeList.forEach((node) => {
      const { id } = node;
      const sinks = {};
      Object.keys(sinkHash[id]).forEach((sink) => {
        const count = layering[id] - layering[sink] - 1;
        let sinkId = sink;
        for (let i = 0; i < count; i += 1) {
          const virtualNode = {
            id: shortid.generate(),
            x: 0,
            y: 0,
            virtual: true,
            neighbors: [sinkId]
          };
          nodes[virtualNode.id] = virtualNode;
          newLayering[virtualNode.id] = newLayering[sink] + 1 + i;
          sinkId = virtualNode.id;
        }
        sinks[sinkId] = true;
      });
      nodes[id] = { ...node, neighbors: Object.keys(sinks) };
    });

    return {
      digraph: new Digraph(Object.values(nodes)),
      layering: newLayering,
    };
  }
}
