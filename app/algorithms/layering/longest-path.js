
import { Layering } from './layering';

export class LongestPathLayering extends Layering {
  get layering() {
    const { sourceHash, nodes } = this.digraph;
    const layering = {};
    const unprocessed = { ...nodes };
    function assignLayer(node, layer) {
      layering[node] = layer;
      delete unprocessed[node];
    }
    Object.keys(sourceHash).filter(node => Object.keys(sourceHash[node]).length === 0).forEach((node) => {
      assignLayer(node, 0);
    });
    let layer = 1;
    const Z = { ...layering };
    while (Object.keys(nodes).length !== Object.keys(layering).length) {
      const current = Object.keys(unprocessed).find(node => Object.keys(sourceHash[node]).every(n => n in Z));
      if (current) {
        assignLayer(current, layer);
      } else {
        layer += 1;
        Object.assign(Z, layering);
      }
    }
    const max = Math.max(...Object.values(layering));
    Object.keys(layering).forEach((node) => {
      layering[node] = max - layering[node];
    });
    return layering;
  }
}
