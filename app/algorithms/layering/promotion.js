
import promote from 'dag-to-layers';
import { Layering } from './layering';

export class PromotionLayering extends Layering {
  get layering() {
    const { edges, nodes, sinkHash, sourceHash } = this.digraph;
    if (edges.length === 0) {
      return { [Object.keys(nodes)[0]]: 0 };
    }
    const layers = promote(
      new Set(Object.keys(nodes)),
      new Map(Object.keys(nodes).map(u => new Set(Object.keys(sourceHash[u]).map(v => [u, v])))),
      new Map(Object.keys(nodes).map(u => new Set(Object.keys(sinkHash[u]).map(v => [u, v]))))
    );
    const layering = {};
    for (let i = 0; i < layers.length; i += 1) {
      const layerNodes = layers[i];
      for (const node of layerNodes) {
        layering[node] = layers.length - 1 - i;
      }
    }
    return layering;
  }
}
