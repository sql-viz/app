
import simplex from 'simplex-solver';
import { Layering } from './layering';
import { VariableNameMap } from '../../models';

export class SimplexLayering extends Layering {
  get layering() {
    const { edges, nodes } = this.digraph;
    if (edges.length === 0) {
      return { [this.digraph.nodeList[0].id]: 0 };
    }
    const map = new VariableNameMap();
    const edgeVertices = Object.keys(nodes);
    const result = simplex.maximize(
      edges.map(({ source, sink }) => `${map.getVariable(source)} - ${map.getVariable(sink)}`).join(' + '),
      [
        ...edges.map(({ source, sink }) => `${map.getVariable(source)} - ${map.getVariable(sink)} >= 1`),
        ...edgeVertices.map(id => `${map.getVariable(id)} >= 0`),
      ]
    );
    const layering = {};
    edgeVertices.forEach((id) => {
      layering[id] = result[map.getVariable(id)];
    });
    return layering;
  }
}
