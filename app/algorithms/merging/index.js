
import { Digraph } from '../../models';

export function merge(digraphs) {
  return new Digraph(digraphs.reduce((nodes, dg) => {
    dg.nodeList.forEach(node => nodes.push(node));
    return nodes;
  }, []));
}
