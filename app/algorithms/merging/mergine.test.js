
import { Digraph } from '../../models';
import { merge } from '.';

describe('merging', () => {
  test('simple', () => {
    const digraph = new Digraph([
      { id: 'A' },
      { id: 'B' }
    ]);
    expect(merge([digraph]).nodeList).toEqual([
      expect.objectContaining({ id: 'A' }),
      expect.objectContaining({ id: 'B' }),
    ]);
  });

  test('threesome', () => {
    const digraphs = [
      new Digraph([
        { id: 'A' },
        { id: 'B' },
      ]),
      new Digraph([
        { id: 'C' },
        { id: 'D' },
        { id: 'E' },
      ]),
      new Digraph([
        { id: 'F' },
      ]),
    ];
    expect(merge(digraphs).nodeList).toEqual([
      expect.objectContaining({ id: 'A' }),
      expect.objectContaining({ id: 'B' }),
      expect.objectContaining({ id: 'C' }),
      expect.objectContaining({ id: 'D' }),
      expect.objectContaining({ id: 'E' }),
      expect.objectContaining({ id: 'F' }),
    ]);
  });
});
