
import { createDictionary } from '../../utils';
import { MagneticCycleRemoval, LongestPathLayering, Sifting, BrandesKopf, restoreEdges, islandify, tiling, merge, TableExpansion } from '../';

const GRID_SIZE = 25;
const TABLE_WIDTH = 12 * GRID_SIZE;
const TITLE_HEIGHT = 2.5 * GRID_SIZE;
const ROW_HEIGHT = 2 * GRID_SIZE;
const VIRTUAL_NODE_SPACING = GRID_SIZE;
const COLUMN_SPACING = 12 * GRID_SIZE;

function calculateTableHeights(digraph, tableKeys) {
  const { nodeList } = digraph;
  return createDictionary(
    nodeList,
    node => node.id,
    node => {
      if (node.virtual) {
        return VIRTUAL_NODE_SPACING;
      }
      return TITLE_HEIGHT + (tableKeys[node.id].length * ROW_HEIGHT);
    },
  );
}

export function sugiyama(digraph, tableKeys) {
  const islands = islandify(digraph);
  const globalLayering = {};
  const tableHeights = {};
  const positioned = islands.map((dg) => {
    const { digraph: acyclic } = (new MagneticCycleRemoval(dg)).solve();

    const { layering, digraph: layered } = (new LongestPathLayering(acyclic)).solve();
    Object.assign(globalLayering, layering);

    const ordering = new Sifting(layered, layering).solve();

    const nodeHeights = calculateTableHeights(layered, tableKeys);
    Object.assign(tableHeights, nodeHeights);
    const bk = new BrandesKopf(layered, layering, ordering, { delta: 100, nodeHeights });

    return bk.solve();
  });
  const mergedDigraph = merge(tiling(positioned, (TABLE_WIDTH * 3) + COLUMN_SPACING));
  const tableExpansion = new TableExpansion(
    mergedDigraph,
    globalLayering,
    tableKeys,
    {
      gridSize: GRID_SIZE,
      rowHeight: ROW_HEIGHT,
      titleHeight: TITLE_HEIGHT,
      tableWidth: TABLE_WIDTH,
      tableHeights,
      columnSpacing: COLUMN_SPACING,
    },
  );
  return tableExpansion.solve();
}
