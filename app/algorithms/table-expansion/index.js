
export class TableExpansion {
  constructor(
    digraph,
    layering,
    tableKeys,
    config = {
      gridSize: undefined,
      tableHeights: undefined,
      rowHeight: undefined,
      titleHeight: undefined,
      tableWidth: undefined,
      columnSpacing: undefined,
    }
  ) {
    this.digraph = digraph;
    this.layering = layering;
    this.config = config;
    this.tableKeys = tableKeys;
    this.cache = {
      tables: null,
      keyPositions: null,
    };
  }

  discretify(value) {
    const { gridSize } = this.config;
    return Math.floor(value / gridSize) * gridSize;
  }

  calculateTables() {
    const { digraph, layering, config } = this;
    const { tableWidth, titleHeight, rowHeight, tableHeights, columnSpacing } = config;
    const { nodeList } = digraph;
    const tables = nodeList.filter(node => !node.virtual).map((node) => {
      const height = tableHeights[node.id];
      const layer = layering[node.id];
      const x = this.discretify((columnSpacing * layer) + (node.x - (tableWidth / 2)));
      const y = this.discretify((node.y - (height / 2)));
      const rows = this.tableKeys[node.id].map((row, idx) => ({
        table: node.id,
        id: row,
        name: row,
        y: titleHeight + (rowHeight / 2) + (idx * rowHeight),
        x: 10,
        width: tableWidth,
        height: rowHeight,
      }));
      return {
        id: node.id,
        dimensions: { width: tableWidth, height, x, y, titleHeight, rowHeight },
        data: { name: node.id, rows },
      };
    });

    return tables;
  }

  solve() {
    return {
      tables: this.calculateTables(),
      config: this.config,
    };
  }
}
