
import { Digraph } from '../../models';

export function tiling(digraphs, delta = 100) {
  let offset = 0;
  return digraphs.map((G, idx, dgs) => {
    const [width] = G.dimensions;
    const dg = new Digraph(G.nodeList.map(node => ({ ...node, x: offset + node.x })));
    offset += width + delta;
    return dg;
  });
}
