
import { Digraph } from '../../models';
import { islandify } from '../islandification';
import { tiling } from '.';

describe('tiling', () => {
  test('simple', () => {
    const dg = new Digraph([
      { id: 'A', x: 0 },
      { id: 'B', x: 100 },
    ]);
    const [shifted] = tiling([dg], 100);
    expect(shifted.nodeList).toEqual([
      expect.objectContaining({ id: 'A', x: 0 }),
      expect.objectContaining({ id: 'B', x: 100 }),
    ]);
  });

  test('tripple', () => {
    const dgs = [
      new Digraph([
        { id: 'A', x: 0 },
        { id: 'B', x: 100 },
      ]),
      new Digraph([
        { id: 'C', x: 0 },
        { id: 'D', x: 100 },
        { id: 'E', x: 200 },
      ]),
      new Digraph([
        { id: 'F', x: 100 },
      ]),
    ];
    const shifted = tiling(dgs, 100);
    expect(shifted.map(dg => dg.nodeList)).toEqual([
      [
        expect.objectContaining({ id: 'A', x: 0 }),
        expect.objectContaining({ id: 'B', x: 100 }),
      ],
      [
        expect.objectContaining({ id: 'C', x: 200 }),
        expect.objectContaining({ id: 'D', x: 300 }),
        expect.objectContaining({ id: 'E', x: 400 }),
      ],
      [
        expect.objectContaining({ id: 'F', x: 600 }),
      ],
    ]);
  });
});
