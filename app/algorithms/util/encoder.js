
export class SimplexEncoder {
  constructor(data) {
    this.decoded = {};
    this.encoded = {};

    data.forEach(({ id }, index) => {
      const x = `x${index}`;
      this.decoded[x] = id;
      this.encoded[id] = x;
    });
  }
}
