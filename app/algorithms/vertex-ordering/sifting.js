
import { ensureHashValue } from '../../utils/hash';
import { VertexOrdering } from './vertex-ordering';

export class Sifting extends VertexOrdering {
  solve() {
    this.cache = { neighborIndexes: {} };
    const { digraph, layering } = this;
    const layerNodeIndexes = this.layerNodeIndexes = {};
    const layerNodes = this.layerNodes = {};
    digraph.nodeList.forEach(({ id }) => {
      const layer = layering[id];
      ensureHashValue(layerNodeIndexes, layer, {});
      ensureHashValue(layerNodes, layer, []);
      layerNodeIndexes[layer][id] = Object.keys(layerNodeIndexes[layering[id]]).length;
      layerNodes[layer].push(id);
    });
    console.log('test', this.crossings(true), this.crossings(false))

    const allNodes = this.nodesSortedByDegree();

    for (let iter = 0; iter < 10; iter += 1) {
      console.log(iter);
      const initialCrossings = this.crossings();
      [true, false].forEach((down) => { //eslint-disable-line
        let optimum = this.crossings(down);
        let crossings = optimum;
        allNodes.forEach((id) => {
          const layer = layering[id];
          const indexes = layerNodeIndexes[layer];
          const nodes = Object.keys(indexes);
          const node = id;
          let optimumIndex = indexes[node];
          let done = false;
          let step = 0;
          let right = true;
          const meta = this.nextNode(node, right);
          right = meta.change ? !right : right;
          let nextNode = meta.next;
          while (!done || indexes[node] !== optimumIndex) {
            if (crossings < optimum) {
              optimum = crossings;
              optimumIndex = indexes[node];
            }
            const cbs = this.calculateCrossing(node, nextNode, down);
            this.swap(node, nextNode);
            const cas = this.calculateCrossing(node, nextNode, down);
            crossings = (crossings - cbs) + cas;
            const { change, next } = this.nextNode(node, right);
            right = change ? !right : right;
            nextNode = next;
            step += 1;
            if (step > 1000) {
              throw new Error('fuuuck');
            }
            if (step === nodes.length) {
              done = true;
            }
          }
        });
      });
      if (this.crossings() >= initialCrossings) {
        console.log('reversing');
        allNodes.reverse();
      }
    }
    console.log('test', this.crossings(true), this.crossings(false), layerNodeIndexes);
    return layerNodeIndexes;
  }

  nodesSortedByDegree() {
    const { nodeList, sinkHash, sourceHash } = this.digraph;
    return nodeList.map(({ id }) => ({ id, degree: Object.keys(sinkHash[id]).length + Object.keys(sourceHash[id]).length }))
      .sort((n1, n2) => n2.degree - n1.degree).map(node => node.id);
  }

  calculateLocalCrossings(u, v) {
    const { layering, layerNodes } = this;
    const layers = Object.keys(layerNodes).length;
    const l = layering[u];
    let crossings = 0;
    if (l !== 0) {
      console.log('not at first layer')
      crossings += this.calculateCrossing(u, v, true);
    }
    if (l !== layers - 1) {
      console.log('not at last layer')
      crossings += this.calculateCrossing(u, v, false);
    }
    return crossings;
  }

  nextNode(u, right) {
    const { layering, layerNodeIndexes, layerNodes } = this;
    const l = layering[u];
    const indexes = layerNodeIndexes[l];
    const nodes = layerNodes[l];
    const index = indexes[u];
    if (right) {
      const change = index === nodes.length - 1;
      const next = nodes[index + (change ? -1 : 1)];
      return { change, next };
    }
    const change = index === 0;
    const next = nodes[index + (change ? 1 : -1)];
    return { change, next };
  }

  swap(u, v) {
    const { layering, layerNodeIndexes, layerNodes } = this;
    const l = layering[u];
    const indexes = layerNodeIndexes[l];
    const nodes = layerNodes[l];
    const ui = indexes[u];
    const vi = indexes[v];
    nodes[ui] = v;
    nodes[vi] = u;
    indexes[u] = vi;
    indexes[v] = ui;
  }

  crossings(down) {
    const { layerNodeIndexes } = this;
    const layers = Object.keys(layerNodeIndexes).length;
    const layerList = Array.from(Array(layers - 1)).map((_, idx) => (down ? (1 + idx) : (layers - 2 - idx)));
    let crossings = 0;
    layerList.forEach((l) => {
      const nodes = Object.keys(layerNodeIndexes[l]);
      for (let i = 0; i < nodes.length - 1; i += 1) {
        const u = nodes[i];
        for (let j = i + 1; j < nodes.length; j += 1) {
          const v = nodes[j];
          crossings += this.calculateCrossing(u, v, down);
        }
      }
    });
    return crossings;
  }

  calculateCrossing(node1, node2, down = true) {
    const { layerNodeIndexes, layering } = this;
    const l = layering[node1];
    const index1 = layerNodeIndexes[l][node1];
    const index2 = layerNodeIndexes[l][node2];
    const u = index1 < index2 ? node1 : node2;
    const v = index1 < index2 ? node2 : node1;
    const uNeighbors = this.getNodeNeighborIndexes(u, down);
    const vNeighbors = this.getNodeNeighborIndexes(v, down);
    return uNeighbors.reduce(
      (crossings, ui) => crossings + vNeighbors.reduce((total, vi) => total + (ui > vi ? 1 : 0), 0),
      0,
    );
  }

  getNodeNeighborIndexes(node, down) {
    const { digraph, layering, layerNodeIndexes } = this;
    const { sinkHash, sourceHash } = digraph;
    const neighborHash = down ? sinkHash : sourceHash;
    return Object.keys(neighborHash[node]).map(n => layerNodeIndexes[layering[n]][n]);
  }
}
