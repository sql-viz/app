
import { ensureHashValue } from '../../utils/hash';
import { VertexOrdering } from './vertex-ordering';

export class Sifting extends VertexOrdering {
  solve() {
    const { digraph, layering } = this;
    const layerNodeIndexes = this.layerNodeIndexes = {};
    const layerNodes = this.layerNodes = {};
    digraph.nodeList.forEach(({ id }) => {
      const layer = layering[id];
      ensureHashValue(layerNodeIndexes, layer, {});
      ensureHashValue(layerNodes, layer, []);
      layerNodeIndexes[layer][id] = layerNodes[layer].length;
      layerNodes[layer].push(id);
    });
    const cm = this.cm = this.buildCrossingMatrix();

    const allNodes = this.nodesSortedByDegree();
    const allNodesReversed = [...allNodes].reverse();
    function* randomNodes(count) {
      for (let i = 0; i < count; i += 1) {
        yield allNodes[Math.floor(allNodes.length * Math.random())];
      }
    }

    const initialCrossins = this.crossings();
    let crossings = initialCrossins;
    console.log(`initial crossings: ${crossings}`);

    const start = Date.now();

    let failCount = 0;
    const nodeSource = [allNodes, allNodesReversed];
    let iterations = 0;
    while (failCount < nodeSource.length) {
      for (const node of nodeSource[failCount]) { // eslint-disable-line no-restricted-syntax
        const l = layering[node];
        const c = cm[l];
        const nodes = layerNodes[l];
        const n = nodes.length;
        const index = layerNodeIndexes[l][node];
        let bestIndex = index;
        //  sift right;
        let i = index;
        for (; i < n - 1; i += 1) {
          if (c[i + 1][i] <= c[i][i + 1]) {
            bestIndex = i + 1;
          }
          this.swap(node, nodes[i + 1]);
        }
        //  sift left
        for (; i > 0; i -= 1) {
          if (c[i][i - 1] <= c[i - 1][i]) {
            bestIndex = i - 1;
          }
          this.swap(node, nodes[i - 1]);
        }
        for (; i !== bestIndex; i += 1) {
          this.swap(node, nodes[i + 1]);
        }
        this.update(node, index, bestIndex);
      }
      const afterCrossing = this.crossings();
      if (afterCrossing >= crossings) {
        failCount += 1;
      } else {
        failCount = 0;
        crossings = afterCrossing;
      }
      iterations += 1;
    }

    const end = Date.now();
    console.log(`crossings: ${crossings} change: ${(100 * ((crossings - initialCrossins) / initialCrossins)).toFixed(2)} %`);
    console.log(`time: ${end - start} ms iterations: ${iterations}`);

    return layerNodeIndexes;
  }

  get layerCount() {
    return Object.keys(this.layerNodeIndexes).length;
  }

  buildCrossingMatrix() {
    const { layerNodes } = this;
    const cm = {};
    Object.keys(layerNodes).forEach((layer) => {
      const nodes = layerNodes[layer];
      cm[layer] = nodes.map((n1) => nodes.map((n2) => this.nodeCrossings(n1, n2, true) + this.nodeCrossings(n1, n2, false)));
    });
    return cm;
  }

  nodeCrossings(u, v, up = true) {
    if (u === v) {
      return 0;
    }
    const { layering, layerCount, layerNodeIndexes } = this;
    const layer = layering[u];
    if ((up && layer === 0) || (!up && layer === layerCount - 1)) {
      return 0;
    }
    const l = layer + (up ? -1 : 1);
    const indexes = layerNodeIndexes[l];
    const uNeighbors = this.getNeighbors(u, up);
    const vNeighbors = this.getNeighbors(v, up);
    const count = uNeighbors.reduce(
      (crossings, un) => crossings + vNeighbors.reduce((total, vn) => total + (indexes[un] > indexes[vn] ? 1 : 0), 0),
      0,
    );
    return count;
  }

  getNeighbors(node, up) {
    const { digraph } = this;
    const { sinkHash, sourceHash } = digraph;
    const neighborHash = up ? sinkHash : sourceHash;
    return Object.keys(neighborHash[node]);
  }

  update(v, start, end) {
    if (start === end) {
      return;
    }
    const { layering, layerNodeIndexes, layerNodes, cm, layerCount } = this;
    const l = layering[v];
    const nodes = layerNodes[l];
    const s = start < end ? start : end + 1;
    const e = start < end ? end : start + 1;
    const inc = start < end ? 1 : -1;
    [true, false].forEach((up) => {
      const ln = l + (up ? -1 : 1);
      if (ln < 0 || ln >= layerCount) {
        return;
      }
      const idx = layerNodeIndexes[ln];
      const cml = cm[ln];
      const vns = this.getNeighbors(v, up);
      for (let i = s; i < e; i += 1) {
        const xns = this.getNeighbors(nodes[i], up);
        vns.forEach((vn) => {
          xns.forEach((xn) => {
            cml[idx[xn]][idx[vn]] -= inc;
            cml[idx[vn]][idx[xn]] += inc;
          });
        });
      }
    });
  }

  swap(u, v) {
    const { layering, layerNodeIndexes, layerNodes, cm } = this;
    const l = layering[u];
    const cml = cm[l];
    const indexes = layerNodeIndexes[l];
    const nodes = layerNodes[l];
    const ui = indexes[u];
    const vi = indexes[v];
    nodes[ui] = v;
    nodes[vi] = u;
    indexes[u] = vi;
    indexes[v] = ui;

    let temp = cml[ui];
    cml[ui] = cml[vi];
    cml[vi] = temp;
    for (let i = 0; i < nodes.length; i += 1) {
      temp = cml[i][ui];
      cml[i][ui] = cml[i][vi];
      cml[i][vi] = temp;
    }
  }

  nodesSortedByDegree() {
    const { nodeList, sinkHash, sourceHash } = this.digraph;
    return nodeList.map(({ id }) => ({ id, degree: Object.keys(sinkHash[id]).length + Object.keys(sourceHash[id]).length }))
      .sort((n1, n2) => n2.degree - n1.degree).map(node => node.id);
  }

  crossings() {
    const { layerCount, layerNodes, cm } = this;
    let crossings = 0;
    for (let l = 0; l < layerCount; l += 2) {
      const cml = cm[l];
      const n = layerNodes[l].length;
      for (let i = 0; i < n - 1; i += 1) {
        for (let j = i + 1; j < n; j += 1) {
          crossings += cml[i][j];
        }
      }
    }
    return crossings;
  }
}
