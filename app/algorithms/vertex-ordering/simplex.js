
import simplex from 'simplex-solver';
import { VertexOrdering } from './vertex-ordering';
import { ensureHashValue } from '../../utils/hash';
import { SimplexEncoder } from '../util';

//  ovo mi pojede svu memoriju.... tolko o egzaktnim rješenjima :S

export class SimplexOrdering extends VertexOrdering {
  solve() {
    const { digraph, layering } = this;
    const { sourceHash } = digraph;
    const coder = new SimplexEncoder(digraph.nodeList);
    const { decoded, encoded } = coder;
    const layerNodes = {};
    Object.keys(layering).forEach((id) => {
      const layer = layering[id];
      ensureHashValue(layerNodes, layer, []);
      layerNodes[layer].push(id);
    });
    const layerIndices = Object.keys(layerNodes).map(layer => Number(layer));
    const firstLayer = Math.min(...layerIndices);
    const lastLayer = Math.max(...layerIndices);
    const objective = [];
    const constraints = [];
    for (let layer = firstLayer; layer < lastLayer; layer += 1) {
      const nodes = layerNodes[layer];
      for (let i = 0; i < nodes.length - 1; i += 1) {
        for (let k = i + 1; k < nodes.length; k += 1) {
          const I = nodes[i];
          const K = nodes[k];
          const sourcesI = Object.keys(sourceHash[I]);
          const sourcesK = Object.keys(sourceHash[K]);
          sourcesI.forEach((J) => {
            sourcesK.forEach((L) => {
              if (J !== L) {
                const c = `-cl${layer}${encoded[I]}${encoded[J]}${encoded[K]}${encoded[L]}`;
                objective.push(c);
                constraints.push(`${c} >= 0`);
                constraints.push(`${c} <= 1`);
                constraints.push(`xl${layer + 1}${encoded[J]}${encoded[L]} - xl${layer}${encoded[I]}${encoded[K]} >= -${c}`);
                constraints.push(`xl${layer + 1}${encoded[J]}${encoded[L]} - xl${layer}${encoded[I]}${encoded[K]} <= ${c}`);
                constraints.push(`xl${layer + 1}${encoded[L]}${encoded[J]} - xl${layer}${encoded[I]}${encoded[K]} >= 1 - ${c}`);
                constraints.push(`xl${layer + 1}${encoded[L]}${encoded[J]} - xl${layer}${encoded[I]}${encoded[K]} <= 1 + ${c}`);
              }
            });
          });
        }
      }
    }
    for (let layer = firstLayer; layer <= lastLayer; layer += 1) {
      const nodes = layerNodes[layer];
      for (let i = 0; i < nodes.length - 2; i += 1) {
        const I = nodes[i];
        for (let j = 0; j < nodes.length - 1; j += 1) {
          const J = nodes[j];
          for (let k = 0; k < nodes.length; k += 1) {
            const K = nodes[k];
            constraints.push(`xl${layer}${encoded[I]}${encoded[J]} + xl${layer}${encoded[J]}${encoded[K]} - xl${layer}${encoded[I]}${encoded[K]} >= 0`);
            constraints.push(`xl${layer}${encoded[I]}${encoded[J]} + xl${layer}${encoded[J]}${encoded[K]} - xl${layer}${encoded[I]}${encoded[K]} <= 1`);
          }
        }
      }
      for (let i = 0; i < nodes.length - 2; i += 1) {
        const I = nodes[i];
        for (let j = 0; j < nodes.length - 1; j += 1) {
          const J = nodes[j];
          constraints.push(`xl${layer}${encoded[I]}${encoded[J]} >= 0`);
          constraints.push(`xl${layer}${encoded[I]}${encoded[J]} <= 1`);
        }
      }
    }
    return simplex.maximize(objective.join(' + '), constraints);
  }
}
