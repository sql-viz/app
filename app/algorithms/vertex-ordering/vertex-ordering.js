
// @flow
import { Digraph } from '../../models';
import { LayeringHash } from '../layering';

export class VertexOrdering {
  constructor(digraph: Digraph, layering: LayeringHash) {
    this.digraph = digraph;
    this.layering = layering;
  }
  solve() {
    throw new Error('not implemented');
  }
}
