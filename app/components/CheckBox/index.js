
import React from 'react';
import classNames from 'classnames';

import styles from './CheckBox.scss';

export const CheckBox = ({ checked, children, onCheck }) => (
  <div
    className={classNames(styles.CheckBox, { [styles.checked]: checked })}
    onClick={onCheck}
  >
    {children}
  </div>
);
