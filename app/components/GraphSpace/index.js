// @flow
import React, { Component } from 'react';
import classnames from 'classnames';
import * as d3 from 'd3';

import { Digraph } from '../../models/Digraph';
import { ensureHashValue } from '../../utils/hash';

import styles from './GraphSpace.scss';

function translate(x, y) {
  return `translate(${x},${y})`;
}

function scale(k) {
  return `scale(${k})`;
}

function classSelect(className) {
  return `.${className}`;
}

type Props = {
  digraph: Digraph,
  layering?: { [key: string]: number }
};

const CANVAS_SIZE = 1000;
const LAYER_SPACING = 500;

export class GraphSpace extends Component<Props> {
  svgRef = (root) => this.rootRef = root;  //  eslint-disable-line
  positions = {};

  get dimensions() {
    const [x, y] = this.centroid;
    const half = CANVAS_SIZE / 2;
    return [x - half, y - half, CANVAS_SIZE, CANVAS_SIZE];
  }

  get digraph() {
    return this.props.digraph;
  }

  get layering() {
    return this.props.layering;
  }

  get ordering() {
    return this.props.ordering;
  }

  get layeringMeta() {
    const counts = {};
    Object.keys(this.layering).forEach((id) => {
      const layer = this.layering[id];
      ensureHashValue(counts, layer, 0);
      counts[layer] += 1;
    });
    return { widths: counts, max: Math.max(...Object.values(counts)) };
  }

  get layerSpacing() {
    return LAYER_SPACING;
  }

  get root() {
    return d3.select(this.rootRef);
  }

  get container() {
    return this.root.select('#container');
  }

  get nodeDrag() {
    return d3.drag()
      .subject(d => d)
      .on('start', this.nodeDragStart)
      .on('drag', this.nodeDragging);
  }

  get nodeDragStart() {
    return function nodeDragStart() {
      d3.event.sourceEvent.stopPropagation();
    };
  }

  get nodeDragging() {
    const self = this;
    const { positions, root } = this;
    return function nodeDragging({ id }) {
      const position = positions[id];
      position.x += d3.event.dx;
      position.y += d3.event.dy;
      d3.select(this).attr('transform', translate(position.x, position.y));
      root.selectAll(`[source-id=${self.attrNodeId(id)}]`)
        .attr('x1', position.x)
        .attr('y1', position.y);
      root.selectAll(`[sink-id=${self.attrNodeId(id)}]`)
        .attr('x2', position.x)
        .attr('y2', position.y);
    };
  }

  get zoom() {
    return d3.zoom()
      .scaleExtent([0.5, 2])
      .on('zoom', this.zooming);
  }

  get zooming() {
    const { container } = this;
    return function zooming() {
      const { k, x, y } = d3.event.transform;
      container.attr('transform', `${translate(x, y)}${scale(k)}`);
    };
  }

  initializeLocalData() {
    const { digraph, layering, ordering } = this;
    this.positions = {};
    if (!layering) {
      digraph.nodeList.forEach(({ id, x, y }) => {
        this.positions[id] = { x, y };
      });
    } else {
      const layeredIndex = {};
      digraph.nodeList.forEach(({ id }) => {
        const layer = layering[id];
        const horizontalSpacing = this.layerSpacing;
        ensureHashValue(layeredIndex, layer, 0);
        const index = ordering ? ordering[layer][id] : layeredIndex[layer];
        this.positions[id] = { x: 0.5 * index * horizontalSpacing, y: layer * this.layerSpacing };
        layeredIndex[layer] += 1;
      });
    }
  }

  attrNodeId(id) {
    return `node-${id}`;
  }

  attrEdgeId({ id }) {
    return `edge-${id}`;
  }

  appendDefinitions() {
    const { root } = this;

    const defs = root.append('defs');

    defs.append('marker')
      .attr('id', 'arrow')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 25)
      .attr('refY', 0)
      .attr('markerWidth', 15)
      .attr('markerHeight', 15)
      .attr('orient', 'auto')
      .append('path')
      .attr('d', 'M0,-5L10,0L0,5');

    defs.append('marker')
      .attr('id', 'arrow-virtual')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 15)
      .attr('refY', 0)
      .attr('markerWidth', 15)
      .attr('markerHeight', 15)
      .attr('orient', 'auto')
      .append('path')
      .attr('d', 'M0,-5L10,0L0,5');
  }

  get centroid() {
    const nodes = Object.values(this.positions);

    function avg(getter) {
      const values = nodes.map(getter);
      if (values.length) {
        return values.reduce((sum, value) => sum + value, 0) / values.length;
      }
      return 0;
    }

    const x = avg(node => node.x);
    const y = avg(node => node.y);
    return [x, y];
  }

  drawEdges() {
    const { container } = this;

    const edges = container.selectAll(classSelect(styles.edge))
      .data(this.digraph.edges)
      .enter()
      .append('g')
      .attr('class', styles.edge);

    edges.append('line')
      .attr('source-id', ({ source }) => this.attrNodeId(source))
      .attr('sink-id', ({ sink }) => this.attrNodeId(sink))
      .attr('x1', ({ source }) => this.positions[source].x)
      .attr('y1', ({ source }) => this.positions[source].y)
      .attr('x2', ({ sink }) => this.positions[sink].x)
      .attr('y2', ({ sink }) => this.positions[sink].y)
      .attr('marker-end', ({ sink }) => (!this.digraph.nodeHash[sink].virtual ? 'url(#arrow)' : undefined));
  }

  nodeClass({ virtual }) {
    return classnames(styles.node, { [styles.virtual]: virtual });
  }

  drawNodes() {
    const { container } = this;

    container.append('circle').attr('r', 5);

    const nodes = container.selectAll(classSelect(styles.node))
      .data(this.digraph.nodeList)
      .enter()
      .append('g')
      .attr('class', n => this.nodeClass(n))
      .attr('id', d => this.attrNodeId(d.id))
      .attr('transform', d => translate(this.positions[d.id].x, this.positions[d.id].y));

    nodes.append('circle')
      .attr('cx', 0)
      .attr('cy', 0)
      .attr('r', 25);

    nodes.append('text')
      .attr('alignment-baseline', 'middle')
      .attr('text-anchor', 'middle')
      .text(n => n.id);
  }

  draw() {
    const { root } = this;
    root.select('#container').remove();

    const container = root.append('g').attr('id', 'container');

    this.appendDefinitions();

    this.drawEdges();

    this.drawNodes();

    container.selectAll(classSelect(styles.node)).call(this.nodeDrag).data(this.digraph.nodeList);
    root.call(this.zoom);
  }

  componentDidMount() {
    this.initializeLocalData();
    this.draw();
  }

  componentDidUpdate() {
    this.initializeLocalData();
    this.draw();
  }

  render() {
    return (
      <svg
        ref={this.svgRef}
        className={styles.GraphSpace}
        viewBox={this.dimensions.join(' ')}
      />
    );
  }
}
