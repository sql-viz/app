
import React from 'react';
import classNames from 'classnames';

import './icon.global.css';

export const Icon = ({ icon, className, ...props }) => <i className={classNames(`icon-${icon}`, className)} {...props}/>;
