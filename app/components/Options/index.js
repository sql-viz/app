
import React, { Component } from 'react';
import { remote } from 'electron';
import { connect } from 'react-redux';
import classNames from 'classnames';

import { Icon } from '../Icon';

import { closeSessionAction } from '../../state/actions';
import { $hasUnsavedChanges, saveDatabaseSnapshotAction, $savingSnapshot } from '../../state/save';
import { undoActionFromHistoryAction, $undoHistoryEmpty } from '../../state/undo';

import styles from './Options.scss';

class OptionsComponent extends Component {
  get className() {
    return classNames(styles.Options);
  }

  close() {
    const { save, hasUnsavedChanges, closeSession } = this.props;
    if (hasUnsavedChanges) {
      const button = remote.dialog.showMessageBox({
        type: 'question',
        title: 'Unsaved changes',
        message: 'You have unsaved changes in your layout. Would you like to save them before you exit?',
        buttons: ['Yes', 'No', 'Cancel'],
      });
      if (button !== 2) {
        if (button === 0) {
          save();
        }
        closeSession();
      }
    } else {
      closeSession();
    }
  }

  render() {
    const { save, closeSession, hasUnsavedChanges, savingSnapshot, undoStackEmpty, undoAction } = this.props;
    return (
      <div className={this.className}>
        <Icon icon='exit' onClick={() => this.close()}/>
        {(!savingSnapshot && hasUnsavedChanges) && <Icon icon='floppy' onClick={save}/>}
        {!undoStackEmpty && <Icon icon='undo' onClick={undoAction}/>}
      </div>
    );
  }
}

export const Options = connect(
  state => ({
    hasUnsavedChanges: $hasUnsavedChanges(state),
    savingSnapshot: $savingSnapshot(state),
    undoStackEmpty: $undoHistoryEmpty(state),
  }),
  {
    save: saveDatabaseSnapshotAction,
    closeSession: closeSessionAction,
    undoAction: undoActionFromHistoryAction,
  }
)(OptionsComponent);
