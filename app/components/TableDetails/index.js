
import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';

import { $tableColumns, $tableReferencedTables, $tableReferencingTables, $tableIndexes } from '../../state/database';
import { $selectedTables, selectTableAction, deselectTableAction } from '../../state/viewBuilder';

import checkIcon from '../../assets/icon/check.svg';
import primaryKeyIcon from '../../assets/icon/key.svg';
import foreignKeyIcon from '../../assets/icon/foreign-key.svg';

import { Tabs } from '../Tabs';
import { CheckBox } from '../CheckBox';
import { Icon } from '../Icon';

import styles from './styles.scss';

class TableDetailsComponent extends Component {
  constructor(...args) {
    super(...args);

    this.state = { view: 'references' };
  }
  get className() {
    return classNames(styles.TableColumns);
  }
  
  renderRow(row) {
    const { columnName, dataType, isNullable, unique, primaryKey, foreignKey } = row;
    let typeIcon = null;
    if (foreignKey) typeIcon = foreignKeyIcon;
    if (primaryKey) typeIcon = primaryKeyIcon;
    return (
      <tr key={columnName}>
        <td className={styles.boolean}>{typeIcon ? <img src={typeIcon} alt='true'/> : null}</td>
        <td className={classNames({ [styles.notNull]: !isNullable })}>{columnName}</td>
        <td>{dataType}</td>
        <td className={styles.boolean}>{unique ? <img src={checkIcon} alt='true'/> : null}</td>
      </tr>
    );
  }

  toggleTable(table) {
    const { selectedTables, selectTable, deselectTable } = this.props;
    if (table in selectedTables) {
      deselectTable(table);
    } else {
      selectTable(table);
    }
  }

  renderReferences() {
    const { referencedTables, referencingTables, selectedTables } = this.props;
    return (
      <div className={styles.References}>
        <div>
          <h2>referenced</h2>
          {referencedTables.map(table => <CheckBox key={table} onCheck={() => this.toggleTable(table)} checked={table in selectedTables}>{table}</CheckBox>)}
        </div>
        <div>
          <h2>referencing</h2>
          {referencingTables.map(table => <CheckBox key={table} onCheck={() => this.toggleTable(table)} checked={table in selectedTables}>{table}</CheckBox>)}
        </div>
      </div>
    );
  }

  renderColumns() {
    const { columns } = this.props;
    return (
      <table>
        <thead>
          <tr>
            <th/>
            <th>name</th>
            <th>type</th>
            <th>unique</th>
          </tr>
        </thead>
        <tbody>
          {columns.map(column => this.renderRow(column))}
        </tbody>
      </table>
    );
  }

  renderIndexes() {
    const { indexes } = this.props;
    return (
      <table className={styles.IndexTable}>
        <thead>
          <tr>
            <th>index</th>
            <th>columns</th>
          </tr>
        </thead>
        <tbody>
          {indexes.map(({ name, columns }) => (
            <tr key={name}>
              <td title={name}>{name}</td>
              <td>{columns}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }

  renderView() {
    const { view } = this.state;
    switch (view) {
      case 'references': return this.renderReferences();
      case 'columns': return this.renderColumns();
      case 'indexes': return this.renderIndexes();
      default: return null;
    }
  }

  render() {
    const { view } = this.state;
    const { table, onCloseClick } = this.props;
    return (
      <div className={this.className}>
        <Icon onClick={onCloseClick} className={styles.close} icon='close'/>
        <h1>{table}</h1>
        <Tabs tabs={['references', 'columns', 'indexes']} selected={view} onSelect={tab => this.setState({ view: tab })}/>
        <div className={styles.ViewContainer}>
          {this.renderView()}
        </div>
      </div>
    );
  }
}

export const TableDetails = connect(
  (state, props) => ({
    columns: $tableColumns(state)[props.table],
    referencedTables: Object.keys($tableReferencedTables(state)[props.table]).sort(),
    referencingTables: Object.keys($tableReferencingTables(state)[props.table]).sort(),
    selectedTables: $selectedTables(state),
    indexes: $tableIndexes(state)[props.table] || [],
  }),
  { selectTable: selectTableAction, deselectTable: deselectTableAction }
)(TableDetailsComponent);
