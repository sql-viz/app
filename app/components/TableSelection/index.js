
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { $tableNames } from '../../state/database';
import {
  $selectedTables,
  selectTableAction,
  deselectTableAction,
  selectAllTablesAction,
  deselectAllTablesAction,
  invertTableSelectionAction,
} from '../../state/viewBuilder';

import { CheckBox } from '../CheckBox';
import { Icon } from '../Icon';

import styles from './TableSelection.scss';

class TableSelectionComponent extends Component {
  toggleTableSelection(table) {
    const { selectedTables, selectTable, deselectTable } = this.props;
    if (table in selectedTables) {
      deselectTable(table);
    } else {
      selectTable(table);
    }
  }
  renderControlPanel() {
    const { deselectAllTables, selectAllTables, invertTableSelection } = this.props;
    return (
      <div className={styles.ControlPanel}>
        <Icon onClick={selectAllTables} icon='tick-inside-circle'/>
        <Icon onClick={deselectAllTables} icon='untick-inside-circle'/>
        <Icon onClick={invertTableSelection} icon='invert'/>
      </div>
    );
  }
  renderTableList() {
    const { tableNames, selectedTables } = this.props;
    return (
      <ul className={styles.TableList}>
        {tableNames.map(table => (
          <li key={table}>
            <CheckBox
              checked={table in selectedTables}
              onCheck={() => this.toggleTableSelection(table)}
            >
              {table}
            </CheckBox>
          </li>
        ))}
      </ul>
    );
  }
  render() {
    return (
      <div className={styles.TableSelection}>
        {this.renderControlPanel()}
        {this.renderTableList()}
      </div>
    );
  }
}

export const TableSelection = connect(
  state => ({
    selectedTables: $selectedTables(state),
    tableNames: $tableNames(state),
  }),
  {
    deselectAllTables: deselectAllTablesAction,
    deselectTable: deselectTableAction,
    invertTableSelection: invertTableSelectionAction,
    selectAllTables: selectAllTablesAction,
    selectTable: selectTableAction,
  }
)(TableSelectionComponent);
