// @flow
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import * as d3 from 'd3';
import { Grid, AStarFinder, Heuristic, Util } from 'pathfinding';
import { Digraph, GridSpace } from '../../models';
import { createDictionary } from '../../utils';
import primaryKeyIcon from '../../assets/icon/key.svg';
import foreignKeyIcon from '../../assets/icon/foreign-key.svg';

import styles from './TableSpace.scss';

function translate(x, y) {
  return `translate(${x},${y})`;
}

function scale(k) {
  return `scale(${k})`;
}

function classSelect(className) {
  return `.${className}`;
}

const CANVAS_SIZE = 1000;
const ICON_SIZE = 0.5;

export class TableSpace extends Component {
  svgRef = (root) => this.rootRef = root;  //  eslint-disable-line
  positions = {};
  tablePositions = {};
  dragging = false;
  cache = {
    keyPositions: null,
  }

  constructor(...args) {
    super(...args);

    this.resolveTableClass = this.resolveTableClass.bind(this);
    this.discretize = this.discretize.bind(this);
    this.calculateLinks = this.calculateLinks.bind(this);
    this.drawEdges = this.drawEdges.bind(this);
    this.drawNodes = this.drawNodes.bind(this);
    this.setViewport = this.setViewport.bind(this);
  }

  get discreteTablePositions() {
    const { tablePositions } = this;
    const discreteTablePositions = {};
    Object.keys(tablePositions).forEach((table) => {
      const { x, y } = tablePositions[table];
      discreteTablePositions[table] = { x: this.discretize(x), y: this.discretize(y) };
    });
    return discreteTablePositions;
  }

  get dimensions() {
    return [0, 0, CANVAS_SIZE, CANVAS_SIZE];
  }

  get tables() {
    return this.props.tables;
  }

  get root() {
    return d3.select(this.rootRef);
  }

  get container() {
    return this.root.select('#container');
  }

  get nodeDrag() {
    return d3.drag()
      .subject(d => d)
      .on('start', this.tableDragStart)
      .on('drag', this.tableDragging)
      .on('end', () => {
        if (this.dragging) {
          const { tablePositions } = this;
          const { onTablesPositionChange } = this.props;
          if (onTablesPositionChange) {
            const newPositions = {};
            Object.keys(tablePositions).forEach((table) => {
              newPositions[table] = { ...tablePositions[table] };
            });
            onTablesPositionChange(newPositions);
          }
          this.dragging = false;
        }
      });
  }

  get tableDragStart() {
    return function nodeDragStart() {
      d3.event.sourceEvent.stopPropagation();
    };
  }

  discretize(value) {
    const { gridSize = 25 } = this.config;
    return Math.floor(value / gridSize) * gridSize;
  }

  get tableDragging() {
    const { tablePositions, discretize, calculateLinks, drawEdges, drawNodes } = this;
    const self = this;
    return function nodeDragging({ id }) {
      self.dragging = true;
      const tablePosition = tablePositions[id];
      const { x: xRawOld, y: yRawOld } = tablePosition;
      tablePosition.x += d3.event.dx;
      tablePosition.y += d3.event.dy;
      const [xOld, yOld, xNew, yNew] = [xRawOld, yRawOld, tablePosition.x, tablePosition.y].map(discretize);
      if (xOld !== xNew || yOld !== yNew) {
        d3.select(this).attr('transform', translate(xNew, yNew));
        const digraph = calculateLinks();
        drawEdges(digraph);
        drawNodes(digraph);
      }
    };
  }

  get zoom() {
    const { onViewportChange } = this.props;
    return d3.zoom()
      .scaleExtent([0.25, 2])
      .on('zoom', this.zooming)
      .on('end', () => {
        if (onViewportChange) {
          const { k, x, y } = d3.event.transform;
          onViewportChange({ k, x, y });
        }
      });
  }

  setViewport(viewport) {
    const { k, x, y } = viewport;
    const { container } = this;
    container.attr('transform', `${translate(x, y)}${scale(k)}`);
  }

  get zooming() {
    const { setViewport } = this;
    return function zooming() {
      const { k, x, y } = d3.event.transform;
      setViewport({ k, x, y });
    };
  }

  initializeLocalData() {
    const { tables } = this;
    const { tablePositions } = this.props;
    this.tablePositions = {};
    tables.forEach(({ id, dimensions: { x, y } }) => {
      this.tablePositions[id] = (tablePositions && (id in tablePositions)) ? { ...tablePositions[id] } : { x, y };
    });
  }

  attrNodeId(id) {
    return `node-${id}`;
  }

  attrEdgeId({ id }) {
    return `edge-${id}`;
  }

  appendDefinitions() {
    const { root } = this;

    const defs = root.append('defs');

    defs.append('marker')
      .attr('id', 'arrow')
      .attr('viewBox', '-10 -10 20 20')
      .attr('refX', 0)
      .attr('refY', 0)
      .attr('markerWidth', 10)
      .attr('markerHeight', 10)
      .append('circle')
      .attr('r', '10')
      .attr('cx', '0')
      .attr('cy', '0');

    defs.append('marker')
      .attr('id', 'arrow-virtual')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 15)
      .attr('refY', 0)
      .attr('markerWidth', 15)
      .attr('markerHeight', 15)
      .attr('orient', 'auto')
      .append('path')
      .attr('d', 'M0,-5L10,0L0,5');
  }

  drawEdges(digraph) {
    const { container } = this;
    const { nodes } = digraph;

    container.selectAll(classSelect(styles.edge)).remove();

    const edges = container.selectAll(classSelect(styles.edge))
      .data(digraph.edges)
      .enter()
      .append('g')
      .attr('class', styles.edge);

    edges.append('line')
      .attr('source-id', ({ source }) => this.attrNodeId(source))
      .attr('sink-id', ({ sink }) => this.attrNodeId(sink))
      .attr('x1', ({ source }) => nodes[source].x)
      .attr('y1', ({ source }) => nodes[source].y)
      .attr('x2', ({ sink }) => nodes[sink].x)
      .attr('y2', ({ sink }) => nodes[sink].y)
      .attr('marker-end', ({ sink }) => (!digraph.nodeHash[sink].virtual ? 'url(#arrow)' : undefined));
  }

  nodeClass({ virtual }) {
    return classnames(styles.node, { [styles.virtual]: virtual });
  }

  drawNodes(digraph) {
    const { container } = this;
    const { nodes: nodeHash } = digraph;

    container.selectAll(classSelect(styles.node)).remove();

    const nodes = container.selectAll(classSelect(styles.node))
      .data(digraph.nodeList)
      .enter()
      .append('g')
      .attr('class', n => this.nodeClass(n))
      .attr('id', d => this.attrNodeId(d.id))
      .attr('transform', d => translate(nodeHash[d.id].x, nodeHash[d.id].y));

    nodes.append('circle')
      .attr('cx', 0)
      .attr('cy', 0);

    nodes.append('text')
      .attr('alignment-baseline', 'middle')
      .attr('text-anchor', 'middle');
  }

  resolveTableClass() {
    return styles.table;
  }

  attrTableId(table) {
    return `table-${table.id}`;
  }

  get primaryKeyFilter() {
    const { tablePrimaryKeys } = this.props;
    return r => r.table in tablePrimaryKeys && r.name in tablePrimaryKeys[r.table];
  }

  get foreignKeyFilter() {
    const { tableForeignKeys } = this.props;
    return r => r.table in tableForeignKeys && r.name in tableForeignKeys[r.table];
  }

  drawTables() {
    const { container, discreteTablePositions } = this;
    const { tableColumns, onTableClick } = this.props;

    const tables = container.selectAll(classSelect(styles.table))
      .data(this.tables)
      .enter()
      .append('g')
      .attr('class', t => this.resolveTableClass(t))
      .attr('id', t => this.attrTableId(t))
      .attr('transform', ({ id }) => translate(discreteTablePositions[id].x, discreteTablePositions[id].y))
      .on('click', t => onTableClick(t.id));

    tables.append('rect')
      .attr('x', 0)
      .attr('y', 0)
      .attr('width', t => t.dimensions.width)
      .attr('height', t => t.dimensions.height);

    tables.append('rect')
      .attr('class', styles.titleRect)
      .attr('x', 0)
      .attr('y', 0)
      .attr('height', t => t.dimensions.titleHeight)
      .attr('width', t => t.dimensions.width)
      .attr('stroke', undefined);

    tables.selectAll('text.columnName')
      .data(t => t.data.rows)
      .enter()
      .append('text')
      .attr('alignment-baseline', 'middle')
      .attr('y', r => r.y - (r.height / 6))
      .attr('x', r => r.height)
      .attr('class', 'columnName')
      .attr('font-size', r => (r.height / 2.5))
      .text(r => r.name)
      .attr('font-weight', (r, idx) => (tableColumns[r.table][idx].isNullable ? 'normal' : 'bold'));

    tables.selectAll('text.columnType')
      .data(t => t.data.rows)
      .enter()
      .append('text')
      .attr('alignment-baseline', 'middle')
      .attr('y', r => r.y + (r.height / 4))
      .attr('x', r => r.height)
      .attr('class', 'columnType')
      .attr('fill', '#555')
      .attr('font-size', r => (r.height / 3))
      .text((r, idx) => tableColumns[r.table][idx].dataType);

    [
      { className: 'primaryKey', filter: this.primaryKeyFilter, icon: primaryKeyIcon },
      { className: 'foreignKey', filter: this.foreignKeyFilter, icon: foreignKeyIcon },
    ].forEach(({ className, filter, icon }) => {
      tables.selectAll(`image.${className}`)
        .data(t => t.data.rows.filter(filter))
        .enter()
        .append('image')
        .attr('x', r => 0.5 * (1 - ICON_SIZE) * r.height)
        .attr('y', r => r.y - (0.5 * ICON_SIZE * r.height))
        .attr('width', r => ICON_SIZE * r.height)
        .attr('height', r => ICON_SIZE * r.height)
        .attr('class', className)
        .attr('xlink:href', icon);
    });

    tables.selectAll('line')
      .data(t => t.data.rows)
      .enter()
      .append('line')
      .attr('stroke', 'black')
      .attr('x1', 0)
      .attr('x2', r => r.width)
      .attr('y1', r => r.y - (r.height / 2))
      .attr('y2', r => r.y - (r.height / 2));

    tables.append('text')
      .attr('alignment-baseline', 'middle')
      .attr('text-anchor', 'middle')
      .attr('y', t => t.dimensions.titleHeight / 2)
      .attr('x', t => t.dimensions.width / 2)
      .attr('font-size', t => t.dimensions.titleHeight / 2.5)
      .text(t => t.data.name);
  }

  draw() {
    const { root, zoom } = this;
    root.select('#container').remove();

    const container = root.append('g').attr('id', 'container');

    this.appendDefinitions();

    this.drawTables();
    const digraph = this.calculateLinks();
    this.drawEdges(digraph);
    this.drawNodes(digraph);

    container.selectAll(classSelect(styles.table)).call(this.nodeDrag).data(this.tables);
    const { viewport } = this.props;
    const { k, x, y } = viewport || { k: 1, x: 0, y: 0 };
    root.call(zoom);
    root.call(zoom.transform, d3.zoomIdentity.translate(x, y).scale(k));
  }

  get keyLinks() {
    return this.props.keyLinks;
  }

  get config() {
    return this.props.config;
  }

  calculateKeyPositions() {
    const { discreteTablePositions: tablePositions, tables } = this;
    const keyPositions = {};
    tables.forEach((table) => {
      const { id, dimensions: { width }, data: { rows } } = table;
      const { x, y } = tablePositions[id];
      Object.assign(
        keyPositions,
        createDictionary(
          rows,
          row => `${id}.${row.id}`,
          row => ({ x: x + (width / 2), y: y + row.y }),
        ),
      );
    });
    return keyPositions;
  }

  get keyPositions() {
    if (!this.cache.keyPositions) {
      this.cache.keyPositions = this.calculateKeyPositions();
    }
    return this.cache.keyPositions;
  }

  get gridSpace() {
    const { gridSize } = this.config;
    return new GridSpace(Object.values(this.keyPositions), { step: gridSize, padding: 10 });
  }

  createTableGrid(grid) {
    const { gridSpace, tables, discreteTablePositions } = this;
    const newGrid = grid.clone();
    tables.forEach(({ id, dimensions: { width, height } }) => {
      const { x, y } = discreteTablePositions[id];
      const [startX, startY] = gridSpace.transform(x, y);
      const [endX, endY] = gridSpace.transform(x + width, y + height);
      for (let i = startX; i <= endX; i += 1) {
        for (let j = startY; j <= endY; j += 1) {
          newGrid.setWalkableAt(i, j, false);
        }
      }
    });
    return newGrid;
  }

  calculateLinks() {
    this.cache.keyPositions = null;

    const { keyLinks, gridSpace, discreteTablePositions: tablePositions, keyPositions } = this;
    const { width, height } = gridSpace;
    const emptyGrid = new Grid(width, height);
    const grid = this.createTableGrid(emptyGrid);
    const { tableWidth } = this.config;
    const explorer = new AStarFinder({
      allowDiagonal: false,
      dontCrossCorners: true,
      heuristic: Heuristic.octile,
    });
    const links = [];
    Object.keys(keyLinks).forEach((sourceTable) => {
      Object.keys(keyLinks[sourceTable]).forEach((destinationTable) => {
        const [{ x: l }, { x: r }] = [tablePositions[sourceTable], tablePositions[destinationTable]];
        const delta = (l < r ? 1 : -1) * (tableWidth / 2);
        keyLinks[sourceTable][destinationTable].forEach(([source, destination]) => {
          const { x: sourceX, y: sourceY } = keyPositions[`${sourceTable}.${source}`];
          const { x: destinationX, y: destinationY } = keyPositions[`${destinationTable}.${destination}`];
          const tempGrid = grid.clone();
          const [startX, startY] = gridSpace.transform(sourceX + delta, sourceY);
          const [endX, endY] = gridSpace.transform(destinationX - delta, destinationY);
          tempGrid.setWalkableAt(startX, startY, true);
          tempGrid.setWalkableAt(endX, endY, true);
          let path;
          let workingGrid;
          workingGrid = grid;
          path = explorer.findPath(startX, startY, endX, endY, tempGrid);
          if (path.length === 0) {
            workingGrid = emptyGrid;
            path = explorer.findPath(startX, startY, endX, endY, emptyGrid.clone());
          }
          const points = Util.smoothenPath(workingGrid, path);
          points.forEach(([xg, yg], idx) => {
            const isVirtual = idx !== 0 && idx !== points.length - 1;
            const [x, y] = gridSpace.reform(xg, yg);
            links.push({
              id: links.length.toString(),
              x: isVirtual ? x : (idx === 0 ? (sourceX + delta) : (destinationX - delta)),
              y: isVirtual ? y : (idx === 0 ? sourceY : destinationY),
              virtual: isVirtual,
              neighbors: idx < points.length - 1 ? [(links.length + 1).toString()] : []
            });
          });
        });
      });
    });
    return new Digraph(links);
  }

  componentWillMount() {
    this.initializeLocalData();
  }

  componentDidMount() {
    this.draw();
  }

  checkRedrawConditions(oldProps, newProps) {
    function tableHash(props) {
      return JSON.stringify(props.tables.map(t => t.id).sort());
    }
    function positionHash(props) {
      return JSON.stringify(props.tablePositions);
    }
    return (
      tableHash(newProps) !== tableHash(oldProps)
      || positionHash(newProps) !== positionHash(oldProps)
    );
  }

  componentDidUpdate(props) {
    if (this.checkRedrawConditions(props, this.props)) {
      this.initializeLocalData();
      this.draw();
    }
  }

  render() {
    return (
      <svg
        ref={this.svgRef}
        className={styles.GraphSpace}
        viewBox={this.dimensions.join(' ')}
      />
    );
  }
}

TableSpace.propTypes = {
  config: PropTypes.object,
  keyLinks: PropTypes.object,
  tables: PropTypes.arrayOf(PropTypes.object),
};
