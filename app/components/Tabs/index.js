
import React from 'react';
import classNames from 'classnames';

import styles from './Tabs.scss';

export const Tabs = ({ tabs, selected, onSelect }) => {
  return (
    <ul className={styles.Tabs}>
      {tabs.map(tab => (
        <li
          key={tab}
          className={classNames(styles.Tab, { [styles.selected]: tab === selected })}
          onClick={() => onSelect(tab)}
        >
          {tab}
        </li>
      ))
      }
    </ul>
  );
};
