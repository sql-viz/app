
import loadFromPostgres from './postgres';
import loadFromMysql from './mysql';
import loadFromMssql from './mssql';

export const vendorLoaders = {
  postgresql: loadFromPostgres,
  mysql: loadFromMysql,
  mssql: loadFromMssql,
};

export function filterUnprovidedConnectionParameters(parameters) {
  const filteredParameters = {};
  Object.keys(parameters).forEach((key) => {
    if (parameters[key]) {
      filteredParameters[key] = parameters[key];
    }
  });
  return filteredParameters;
}

export const vendorDefaults = {
  postgresql: {
    host: '127.0.0.1',
    port: '5432',
    user: 'postgres'
  },
  mysql: {
    host: '127.0.0.1',
    port: '3306',
    user: 'root'
  },
  mssql: {
    host: '127.0.0.1',
    port: '1433',
    user: 'sa'
  },
};
