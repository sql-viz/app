
import { ConnectionPool } from 'mssql';

export default async function loadFromSqlServer({ host, port, user, database, password }) {
  const pool = new ConnectionPool({
    user,
    password,
    server: host,
    port: Number(port),
    database,
    options: {
      encrypt: false,
    }
  });
  
  await pool.connect();

  const query = (q) => pool.request().query(q);

  const [columnData, primaryKeyData, foreignKeyData, uniqueColumnData, indexData] = await Promise.all([
    query(`
      SELECT
        table_name,
        column_name,
        is_nullable,
        data_type
      FROM INFORMATION_SCHEMA.COLUMNS
      WHERE TABLE_SCHEMA = 'dbo';
    `),
    query(`
      SELECT
        t.name AS table_name,
        tc.name AS column_name
      FROM
        sys.schemas s
        INNER JOIN sys.tables t   ON s.schema_id=t.schema_id
        INNER JOIN sys.indexes i  ON t.object_id=i.object_id
        INNER JOIN sys.index_columns ic ON i.object_id=ic.object_id and i.index_id=ic.index_id
        INNER JOIN sys.columns tc on ic.object_id=tc.object_id and ic.column_id=tc.column_id
      WHERE i.is_primary_key=1 AND s.name = 'dbo';
    `),
    query(`
      SELECT
        OBJECT_NAME(f.parent_object_id) table_name,
        COL_NAME(fc.parent_object_id, fc.parent_column_id) column_name,
        OBJECT_NAME (f.referenced_object_id) foreign_table_name,
        COL_NAME(fc.referenced_object_id, fc.referenced_column_id) foreign_column_name
      FROM sys.foreign_keys AS f
      INNER JOIN sys.foreign_key_columns AS fc ON f.object_id = fc.constraint_object_id
      WHERE SCHEMA_NAME(f.schema_id) = 'dbo';
    `),
    query(`
      SELECT
        TC.table_name,
        column_name
      FROM information_schema.table_constraints TC
      INNER JOIN information_schema.constraint_column_usage CC ON TC.Constraint_Name = CC.Constraint_Name
      WHERE TC.constraint_type = 'UNIQUE' AND TC.TABLE_SCHEMA = 'dbo';
    `),
    query(`
      SELECT 
        t.name AS table_name,
        ind.name AS index_name,
        STRING_AGG (col.name, ', ') WITHIN GROUP (ORDER BY ic.key_ordinal DESC) AS columns
      FROM sys.indexes ind 
      INNER JOIN sys.index_columns ic ON  ind.object_id = ic.object_id and ind.index_id = ic.index_id 
      INNER JOIN sys.columns col ON ic.object_id = col.object_id and ic.column_id = col.column_id 
      INNER JOIN sys.tables t ON ind.object_id = t.object_id 
      WHERE t.is_ms_shipped = 0 AND SCHEMA_NAME(t.schema_id) = 'dbo'
      GROUP BY t.name, ind.name;
    `),
  ]);

  await pool.close();

  return {
    foreignKeyData: foreignKeyData.recordset,
    columnData: columnData.recordset,
    primaryKeyData: primaryKeyData.recordset,
    uniqueColumnData: uniqueColumnData.recordset,
    indexData: indexData.recordset,
  };
}
