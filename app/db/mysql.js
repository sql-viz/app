
import mysql from 'mysql';

function schemaFilter(schema, prefix) {
  const tableSchemaField = prefix ? `${prefix}.TABLE_SCHEMA` : 'TABLE_SCHEMA';
  return `${tableSchemaField} = '${schema}'`;
}

export default function loadFromMysql(data) {
  const connection = mysql.createConnection(data);
  const { database } = data;

  connection.connect();

  function promisifyQuery(query) {
    return new Promise((resolve, reject) => {
      connection.query(query, (error, results) => {
        if (error) reject(error);
        else resolve(results);
      });
    });
  }

  return Promise.all([
    promisifyQuery(`
      SELECT
        TABLE_NAME AS table_name,
        COLUMN_NAME as column_name,
        DATA_TYPE AS data_type,
        IS_NULLABLE AS is_nullable
      FROM information_schema.columns
      WHERE ${schemaFilter(database)};
    `),
    promisifyQuery(`
      SELECT
        k.table_name,
        k.column_name
      FROM information_schema.table_constraints t
      JOIN information_schema.key_column_usage k
      USING(constraint_name,table_schema,table_name)
      WHERE
        t.constraint_type='PRIMARY KEY'
        AND ${schemaFilter(database, 't')};
    `),
    promisifyQuery(`
      SELECT 
        TABLE_NAME AS table_name,
        COLUMN_NAME AS column_name,
        REFERENCED_TABLE_NAME AS foreign_table_name,
        REFERENCED_COLUMN_NAME AS foreign_column_name
      FROM
        INFORMATION_SCHEMA.KEY_COLUMN_USAGE
      WHERE
        ${schemaFilter(database)}
        AND REFERENCED_TABLE_NAME IS NOT NULL;
    `),
    promisifyQuery(`
      SELECT
        k.table_name,
        k.column_name
      FROM information_schema.table_constraints t
      JOIN information_schema.key_column_usage k
      USING(constraint_name,table_schema,table_name)
      WHERE
        t.constraint_type='UNIQUE'
        AND ${schemaFilter(database, 't')};
    `),
    promisifyQuery(`
      SELECT
        table_name,
        index_name,
        GROUP_CONCAT(column_name ORDER BY seq_in_index) AS columns
      FROM information_schema.statistics
      WHERE ${schemaFilter(database)}
      GROUP BY 1,2;
    `),
  ]).then(([columnData, primaryKeyData, foreignKeyData, uniqueColumnData, indexData]) => {
    connection.end();
    return { foreignKeyData, columnData, primaryKeyData, uniqueColumnData, indexData, };
  });
}
