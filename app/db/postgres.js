
import db from 'pg-client';

export const LOAD_DATABASE_META_START = 'LOAD_DATABASE_META_START';
export const LOAD_DATABASE_META_SUCCESS = 'LOAD_DATABASE_META_SUCCESS';

function pgSchemaFilter(prefix, schemaField = 'table_schema') {
  const tableSchemaField = prefix ? `${prefix}.${schemaField}` : schemaField;
  return `${tableSchemaField} != 'information_schema' AND ${tableSchemaField} NOT LIKE 'pg\\_%'`;
}

export default function loadDatabaseMetaAction({ host, port, user, database }) {
  return new Promise((resolve) => {
    const connectionString = `postgresql://${user}@${host}:${port}/${database}`;

    db.connect(connectionString, async ({ query }) => {
      const [columns, primaryKeys, foreignKeys, uniques, indexes] = await Promise.all([
        query(`
        SELECT
          table_schema,
          table_name,
          column_name,
          data_type,
          is_nullable
        FROM information_schema.columns
        WHERE ${pgSchemaFilter()};
        `),
        query(`
        SELECT t.table_name, c.column_name
        FROM information_schema.key_column_usage AS c
        LEFT JOIN information_schema.table_constraints AS t
        ON t.constraint_name = c.constraint_name
        WHERE
          t.constraint_type = 'PRIMARY KEY'
          AND ${pgSchemaFilter('t')};
        `),
        query(`
        SELECT
          tc.table_name,
          kcu.column_name,
          ccu.table_name AS foreign_table_name,
          ccu.column_name AS foreign_column_name 
        FROM 
          information_schema.table_constraints AS tc 
          JOIN information_schema.key_column_usage AS kcu
            ON tc.constraint_name = kcu.constraint_name
          JOIN information_schema.constraint_column_usage AS ccu
            ON ccu.constraint_name = tc.constraint_name
        WHERE
          constraint_type = 'FOREIGN KEY'
          AND ${pgSchemaFilter('tc')};
        `),
        query(`
        SELECT t.table_name, c.column_name
        FROM information_schema.key_column_usage AS c
        LEFT JOIN information_schema.table_constraints AS t
        ON t.constraint_name = c.constraint_name
        WHERE
          t.constraint_type = 'UNIQUE'
          AND ${pgSchemaFilter('t')};
        `),
        query(`
        SELECT
            t.relname as table_name,
            i.relname as index_name,
            array_to_string(array_agg(a.attname), ', ') as columns
        FROM
            pg_class t,
            pg_class i,
            pg_index ix,
            pg_attribute a
        WHERE
            t.oid = ix.indrelid
            and i.oid = ix.indexrelid
            and a.attrelid = t.oid
            and a.attnum = ANY(ix.indkey)
            and t.relkind = 'r'
        GROUP BY
            t.relname,
            i.relname
        ORDER BY
            t.relname,
            i.relname;
        `),
      ]);
      resolve({
        foreignKeyData: foreignKeys.rows,
        columnData: columns.rows,
        primaryKeyData: primaryKeys.rows,
        uniqueColumnData: uniques.rows,
        indexData: indexes.rows,
      });
    });
  });
}
