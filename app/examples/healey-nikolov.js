
import { Digraph } from '../models';

export const healeyNikolov = new Digraph(
  [
    { id: '1', neighbors: ['3'] },
    { id: '2', neighbors: ['3', '4'] },
    { id: '3', neighbors: ['5', '6'] },
    { id: '4', neighbors: ['6'] },
    { id: '5', neighbors: ['7'] },
    { id: '6', neighbors: ['7', '8', '10'] },
    { id: '7', neighbors: ['9', '10', '11'] },
    { id: '8', neighbors: ['11'] },
    { id: '9', neighbors: ['12', '13'] },
    { id: '10', neighbors: ['14'] },
    { id: '11', neighbors: ['14', '15'] },
    { id: '12', neighbors: [] },
    { id: '13', neighbors: [] },
    { id: '14', neighbors: ['1', '2'] },
    { id: '15', neighbors: ['1', '5'] },
  ],
  {
    x: () => 1000 * Math.random(),
    y: () => 1000 * Math.random(),
  }
);
