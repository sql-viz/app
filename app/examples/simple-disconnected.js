
import { Digraph } from '../models';

export const simpleDiconnected = new Digraph([
  { id: 'A', neighbors: ['B'] },
  { id: 'B' },
  { id: 'C', neighbors: ['D', 'E', 'F'] },
  { id: 'D' },
  { id: 'E' },
  { id: 'F' },
  { id: 'G', neighbors: ['H'] },
  { id: 'H', neighbors: ['I'] },
  { id: 'I' },
  { id: 'J' },
]);
