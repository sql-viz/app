
import { ensureHashValue } from '../utils/hash';

export class Digraph {
  constructor(nodes = [], { id = node => node.id, x = node => node.x, y = node => node.y } = {}) {
    this.nodes = {};
    this.cache = {};
    nodes.forEach((node) => {
      const nodeId = id(node);
      this.nodes[nodeId] = {
        ...node,
        neighbors: Array.from(new Set(node.neighbors || []).values()),
        id: nodeId,
        x: x(node),
        y: y(node),
      };
    });
  }

  get nodeList() {
    return Object.values(this.nodes);
  }

  get nodeHash() {
    return this.nodes;
  }

  getNode(id) {
    if (id in this.nodes) {
      return this.nodes[id];
    }
    throw new Error(`node ${id} is not a member of the digraph`);
  }

  getNeighborList(id) {
    return this.getNode(id).neighbors.map(neighborId => this.getNode(neighborId));
  }

  getNeighbors(id) {
    const neighborList = this.getNeighborList(id);
    const neighbors = {};
    neighborList.forEach((neighbor) => {
      neighbors[neighbor.id] = neighbor;
    });
    return neighbors;
  }

  get edges() {
    if (!('edges' in this.cache)) {
      const nodeEdges = this.nodeList.map((node) => {
        const { x, y } = node;
        const neighbors = this.getNeighborList(node.id);
        return neighbors.map(neighbor => ({
          source: node.id,
          sink: neighbor.id,
          x1: x,
          y1: y,
          x2: neighbor.x,
          y2: neighbor.y,
        }));
      });

      this.cache.edges = [].concat(...nodeEdges);
    }
    return this.cache.edges;
  }

  get sinkHash() {
    if (!('sinkHash' in this.cache)) {
      const sinkHash = {};
      this.nodeList.forEach((node) => {
        sinkHash[node.id] = this.getNeighbors(node.id);
      });
      this.cache.sinkHash = sinkHash;
    }
    return this.cache.sinkHash;
  }

  get sourceHash() {
    if (!('sourceHash' in this.cache)) {
      const sourceHash = {};
      this.nodeList.forEach((node) => {
        ensureHashValue(sourceHash, node.id, {});
        this.getNeighborList(node.id).forEach((neighbor) => {
          ensureHashValue(sourceHash, neighbor.id, {});
          sourceHash[neighbor.id][node.id] = node;
        });
      });
      this.cache.sourceHash = sourceHash;
    }
    return this.cache.sourceHash;
  }

  calculateSize(accessor) {
    const { nodeList } = this;
    const min = Math.min(...nodeList.map(node => accessor(node)));
    const max = Math.max(...nodeList.map(node => accessor(node)));
    return max - min;
  }

  get dimensions() {
    return [
      this.calculateSize(node => node.x),
      this.calculateSize(node => node.y),
    ];
  }
}
