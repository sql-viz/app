
function calculateExtreme(fn, items, field) {
  return fn(...items.map(field));
}

class GridSpace {
  constructor(items, { x = a => a.x, y = a => a.y, step = 1, padding = 0 } = {}) {
    this.step = step;
    this.padding = padding;
    this.minX = calculateExtreme(Math.min, items, x);
    this.maxX = calculateExtreme(Math.max, items, x);
    this.minY = calculateExtreme(Math.min, items, y);
    this.maxY = calculateExtreme(Math.max, items, y);
    this.width = (2 * padding) + Math.ceil(((this.maxX - this.minX) + 1) / step);
    this.height = (2 * padding) + Math.ceil(((this.maxY - this.minY) + 1) / step);
  }

  transform(x, y) {
    const { minX, minY } = this;
    return [this.transformOne(x, minX), this.transformOne(y, minY)];
  }

  transformOne(value, min) {
    const { padding, step } = this;
    return padding + Math.round((value - min) / step);
  }

  reform(x, y) {
    const { minX, minY } = this;
    return [this.reformOne(x, minX), this.reformOne(y, minY)];
  }

  reformOne(value, min) {
    const { padding, step } = this;
    return min + ((value - padding) * step);
  }
}

exports.GridSpace = GridSpace;
