
export class VariableNameMap {
  constructor() {
    this.variableToName = {};
    this.nameToVariable = {};
  }
  get nextVariable() {
    return `x${Object.keys(this.nameToVariable).length}`;
  }
  getVariable(name) {
    if (!(name in this.nameToVariable)) {
      const variable = this.nextVariable;
      this.nameToVariable[name] = variable;
      this.variableToName[variable] = name;
    }
    return this.nameToVariable[name];
  }
  getName(variable) {
    return this.variableToName[variable];
  }
}
