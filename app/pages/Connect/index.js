
import React, { Component } from 'react';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { vendorLoaders, vendorDefaults } from '../../db';
import { $databaseDataLoading, $databaseDataEmpty, loadDatabaseMetaAction } from '../../state/database';

import { Icon } from '../../components';

import styles from './Connect.scss';

class ConnectPageComponent extends Component {
  constructor(...args) {
    super(...args);

    this.state = {
      form: {
        host: '127.0.0.1',
        port: '',
        user: '',
        database: '',
        password: '',
        vendor: 'postgresql',
        ...vendorDefaults.postgresql,
      },
      touched: {},
    };

    this.onChange = this.onChange.bind(this);
    this.connect = this.connect.bind(this);
  }
  componentDidUpdate(props) {
    if (props.loading && !this.props.loading && !this.props.empty) {
      this.props.history.push('/');
    }
  }
  get className() {
    const { loading } = this.props;
    return classNames(
      styles.ConnectPage,
      { [styles.loading]: loading },
    );
  }
  onChange({ target: { name, value } }) {
    const { form, touched } = this.state;
    this.setState({ form: { ...form, [name]: value }, touched: { ...touched, [name]: true } });
  }
  connect() {
    const { connectToDatabase } = this.props;
    const { form } = this.state;
    connectToDatabase(form);
  }
  renderBackButton() {
    return <div className={styles.BackButtonContainer}><Link to='/'><Icon icon='back'/></Link></div>;
  }
  renderForm() {
    const { host, port, user, password, database } = this.state.form;
    return (
      <div className={classNames(styles.Form)}>
        <div className={styles.Labels}>
          <label htmlFor='host'>host</label>
          <label htmlFor='port'>port</label>
          <label htmlFor='user'>user</label>
          <label htmlFor='password'>password</label>
          <label htmlFor='database'>database</label>
        </div>
        <div className={styles.Inputs}>
          <input autoFocus name='host' type='text' value={host} onChange={this.onChange}/>
          <input name='port' type='text' value={port} onChange={this.onChange}/>
          <input name='user' type='text' value={user} onChange={this.onChange}/>
          <input name='password' type='password' value={password} onChange={this.onChange}/>
          <input name='database' type='text' value={database} onChange={this.onChange}/>
        </div>
      </div>
    );
  }
  setVendor(vendor) {
    const { form, touched } = this.state;
    const newForm = { ...form, vendor };
    const defaults = vendorDefaults[vendor];
    Object.keys(defaults).forEach((key) => {
      if (!(key in touched)) {
        newForm[key] = defaults[key];
      }
    });
    this.setState({ form: newForm });
  }
  renderVendorOptions() {
    const { vendor: selectedVendor } = this.state.form;
    return (
      <div className={styles.VendorOptions}>
        {Object.keys(vendorLoaders).map(vendor => (
          <div
            key={vendor}
            className={classNames(styles.Option, { [styles.selected]: vendor === selectedVendor })}
            onClick={() => this.setVendor(vendor)}
          >
            <h2>{vendor}</h2>
            <Icon icon={vendor}/>
          </div>
        ))}
      </div>
    );
  }
  renderControls() {
    return (
      <div className={styles.Controls}>
        <button onClick={this.connect}>connect</button>
      </div>
    );
  }
  renderLoader() {
    return (
      <div className={styles.Loader}>connecting...</div>
    );
  }
  render() {
    return (
      <div className={this.className}>
        {this.renderBackButton()}
        {this.renderVendorOptions()}
        {this.renderForm()}
        {this.renderControls()}
        {this.renderLoader()}
      </div>
    );
  }
}

export const ConnectPage = connect(
  state => ({
    loading: $databaseDataLoading(state),
    empty: $databaseDataEmpty(state),
  }),
  { connectToDatabase: loadDatabaseMetaAction }
)(ConnectPageComponent);
