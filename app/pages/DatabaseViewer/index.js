
import React, { Component } from 'react';
import { connect } from 'react-redux';
import path from 'path';
import { TableSpace, TableSelection, TableDetails, Options } from '../../components';
import { sugiyama } from '../../algorithms';
import { closeSessionAction } from '../../state/actions';
import {
  $selectedTableColumnNames,
  $selectedTableKeyLinks,
  $selectedTableRelationDigraph,
  $selectedTables,
  selectTableAction,
  deselectTableAction,
  $pinnedTable,
  pinTableAction,
  unpinTableAction,
  $tablePositions,
  overrideTablePositionsAction,
  $viewport,
  overrideViewportAction,
} from '../../state/viewBuilder';
import {
  $tableNames,
  $tablePrimaryKeys,
  $tableForeignKeys,
  $tableColumns,
} from '../../state/database';
import { saveDatabaseSnapshotAction, $filePath } from '../../state/save';

import styles from './DatabaseViewer.scss';

class DatabaseViewerComponent extends Component {
  renderGraph() {
    const {
      tableKeys,
      keyLinks,
      relationalDigraph,
      selectedTables,
      tablePrimaryKeys,
      tableForeignKeys,
      tableColumns,
      pinTable,
      tablePositions,
      overrideTablePositions,
      viewport,
      overrideViewport,
    } = this.props;
    if (Object.keys(relationalDigraph.nodes).length === 0) return null;

    const { tables, config } = sugiyama(relationalDigraph, tableKeys, keyLinks);
    return (
      <TableSpace
        tables={tables}
        config={config}
        keyLinks={keyLinks}
        tablePrimaryKeys={tablePrimaryKeys}
        tableForeignKeys={tableForeignKeys}
        tableColumns={tableColumns}
        onTableClick={pinTable}
        tablePositions={tablePositions}
        onTablesPositionChange={overrideTablePositions}
        onViewportChange={overrideViewport}
        viewport={viewport}
      />
    );
  }
  toggle(id) {
    const { selectedTables, selectTable, deselectTable } = this.props;
    if (id in selectedTables) {
      deselectTable(id);
    } else {
      selectTable(id);
    }
  }
  renderTables() {
    return (
      <div className={styles.TableListContainer}>
        <TableSelection/>
      </div>
    );
  }
  renderPinnedTable() {
    const { pinnedTable, unpinTable } = this.props;
    if (!pinnedTable) return null;

    return (
      <div className={styles.PinnedTableContainer}>
        <TableDetails table={pinnedTable} onCloseClick={unpinTable}/>
      </div>
    );
  }
  renderOptions() {
    const { save, closeSession } = this.props;
    return (
      <div className={styles.OptionsContainer}>
        <Options onSaveClick={save} onCloseClick={closeSession}/>
      </div>
    );
  }
  renderFileName() {
    const { filePath } = this.props;
    if (!filePath) return null;

    return (
      <div className={styles.FileNameContainer}>
        <h1>{path.basename(filePath)}</h1>
      </div>
    );
  }
  render() {
    return (
      <div className={styles.DatabaseViewer}>
        {this.renderGraph()}
        {this.renderFileName()}
        {this.renderPinnedTable()}
        {this.renderOptions()}
        {this.renderTables()}
      </div>
    );
  }
}

export const DatabaseViewer = connect(
  state => ({
    keyLinks: $selectedTableKeyLinks(state),
    pinnedTable: $pinnedTable(state),
    relationalDigraph: $selectedTableRelationDigraph(state),
    selectedTables: $selectedTables(state),
    tableColumns: $tableColumns(state),
    tableForeignKeys: $tableForeignKeys(state),
    tableKeys: $selectedTableColumnNames(state),
    tableNames: $tableNames(state),
    tablePrimaryKeys: $tablePrimaryKeys(state),
    filePath: $filePath(state),
    tablePositions: $tablePositions(state),
    viewport: $viewport(state),
  }),
  {
    deselectTable: deselectTableAction,
    pinTable: pinTableAction,
    unpinTable: unpinTableAction,
    selectTable: selectTableAction,
    save: saveDatabaseSnapshotAction,
    closeSession: closeSessionAction,
    overrideTablePositions: overrideTablePositionsAction,
    overrideViewport: overrideViewportAction,
  }
)(DatabaseViewerComponent);
