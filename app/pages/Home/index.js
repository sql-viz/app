
import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { remote } from 'electron';

import { history } from '../../store/configureStore';

import { Icon } from '../../components';

import { loadDatabaseMetaAction } from '../../state/database';
import { $loadingSnapshot, loadDatabaseSnapshotAction } from '../../state/save';

import styles from './Home.scss';

class HomePageComponent extends Component {
  get className() {
    return classNames(styles.Home);
  }
  onConnectClick() {
    history.push('/connect');
  }
  onLoadSnapshotClick() {
    const [path] = remote.dialog.showOpenDialog({ filters: [{ name: 'SQLViz snapshot', extensions: ['json.gz'] }] });
    if (path) {
      const { loadDatabaseSnapshot } = this.props;
      loadDatabaseSnapshot(path);
    }
  }
  render() {
    const { loading } = this.props;
    if (loading) return null;
    return (
      <div className={this.className}>
        <div className={styles.Option} onClick={() => this.onConnectClick()}>
          <Icon icon='db-connect'/>
          <h2>connect</h2>
        </div>
        <div className={styles.Option} onClick={() => this.onLoadSnapshotClick()}>
          <Icon icon='open-folder'/>
          <h2>open</h2>
        </div>
      </div>
    );
  }
}

export const HomePage = connect(
  state => ({
    loading: $loadingSnapshot(state),
  }),
  {
    loadDatabaseSnapshot: loadDatabaseSnapshotAction,
    connectToDatabase: loadDatabaseMetaAction,
  }
)(HomePageComponent);
