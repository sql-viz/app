// @flow
import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import { CLOSE_SESSION } from '../state/actions';
import { databaseReducer } from '../state/database';
import { viewBuilderReducer } from '../state/viewBuilder';
import { saveReducer } from '../state/save';
import { undoReducer } from '../state/undo';

const rootReducer = combineReducers({
  database: databaseReducer,
  viewBuilder: viewBuilderReducer,
  save: saveReducer,
  undo: undoReducer,
  router,
});

export default function wrappedRootReducer(state, action) {
  const { type } = action;
  return rootReducer(type !== CLOSE_SESSION ? state : undefined, action);
}
