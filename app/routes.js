/* eslint flowtype-errors/show-errors: 0 */
import React from 'react';
import { connect } from 'react-redux';
import { Switch, Route, Router } from 'react-router';
import { HomePage, DatabaseViewer, ConnectPage } from './pages';

import { $databaseDataEmpty } from './state/database';

const Routes = ({ history, empty }) => (
  <Router history={history}>
    <Switch>
      <Route path="/connect" component={ConnectPage}/>
      <Route path="/" component={empty ? HomePage : DatabaseViewer}/>
    </Switch>
  </Router>
);

export default connect(state => ({ empty: $databaseDataEmpty(state) }))(Routes);
