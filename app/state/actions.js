
export const CLOSE_SESSION = 'CLOSE_SESSION';

export function closeSessionAction() {
  return { type: CLOSE_SESSION };
}
