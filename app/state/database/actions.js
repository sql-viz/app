
import { vendorLoaders, filterUnprovidedConnectionParameters } from '../../db';

export const LOAD_DATABASE_META_START = 'LOAD_DATABASE_META_START';
export const LOAD_DATABASE_META_SUCCESS = 'LOAD_DATABASE_META_SUCCESS';

export function loadDatabaseMetaAction(connectionData) {
  const { vendor, ...data } = connectionData;
  return dispatch => {
    dispatch({ type: LOAD_DATABASE_META_START });
    return vendorLoaders[vendor](filterUnprovidedConnectionParameters(data)).then(metadata => dispatch({
      type: LOAD_DATABASE_META_SUCCESS,
      payload: metadata,
    }));
  };
}
