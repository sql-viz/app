
import { LOAD_DATABASE_META_START, LOAD_DATABASE_META_SUCCESS } from './actions';
import { LOAD_DATABASE_SNAPSHOT_END } from '../save/actions';

const initialState = {
  loading: false,
  empty: true,
  foreignKeyData: [],
  columnData: [],
  primaryKeyData: [],
  uniqueColumnData: [],
};

export function databaseReducer(state = initialState, { type, payload }) {
  switch (type) {
    case LOAD_DATABASE_META_START: return { ...state, loading: true };
    case LOAD_DATABASE_META_SUCCESS: {
      const { foreignKeyData, columnData, primaryKeyData, uniqueColumnData, indexData } = payload;
      return {
        ...state,
        loading: false,
        empty: false,
        foreignKeyData: foreignKeyData.map(({
          table_name: tableName,
          column_name: columnName,
          foreign_table_name: foreignTable,
          foreign_column_name: foreignColumn,
        }) => ({ tableName, columnName, foreignTable, foreignColumn })),
        columnData: columnData.map(({
          table_name: tableName,
          column_name: columnName,
          data_type: dataType,
          is_nullable: isNullable,
        }) => ({ tableName, columnName, dataType, isNullable: isNullable === 'YES' })),
        primaryKeyData: primaryKeyData.map(({
          table_name: tableName,
          column_name: columnName,
        }) => ({ tableName, columnName })),
        uniqueColumnData: uniqueColumnData.map(({
          table_name: tableName,
          column_name: columnName,
        }) => ({ tableName, columnName })),
        indexData: indexData.map(({
          table_name: tableName,
          index_name: name,
          columns,
        }) => ({ tableName, name, columns })),
      };
    }
    case LOAD_DATABASE_SNAPSHOT_END: return { ...state, ...payload.database, loading: false, empty: false };
    default: return state;
  }
}
