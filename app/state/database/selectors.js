
import { createSelector } from 'reselect';
import { createDictionary, createGroupingDictionary, ensureHashValue } from '../../utils';

export const $database = state => state.database;

const $tableColumnData = createSelector(
  $database,
  database => database.columnData,
);

export const $foreignKeyData = createSelector(
  $database,
  database => database.foreignKeyData
);

const $primaryKeyData = createSelector(
  $database,
  database => database.primaryKeyData,
);

const $uniqueColumnData = createSelector(
  $database,
  database => database.uniqueColumnData,
);

const $indexData = createSelector(
  $database,
  database => database.indexData,
);

export const $databaseDataLoading = createSelector(
  $database,
  database => database.loading,
);

export const $databaseDataEmpty = createSelector(
  $database,
  database => database.empty,
);

export const $tablePrimaryKeys = createSelector(
  $primaryKeyData,
  (primaryKeyData) => {
    const primaryKeys = {};
    primaryKeyData.forEach(({ tableName, columnName }) => {
      ensureHashValue(primaryKeys, tableName, {});
      primaryKeys[tableName][columnName] = true;
    });
    return primaryKeys;
  }
);

export const $tableForeignKeys = createSelector(
  $tablePrimaryKeys,
  $foreignKeyData,
  (tablePrimaryKeys, foreignKeyData) => {
    const foreignKeys = {};
    foreignKeyData.forEach(({ tableName, columnName }) => {
      if (!(tableName in tablePrimaryKeys) || !(columnName in tablePrimaryKeys[tableName])) {
        ensureHashValue(foreignKeys, tableName, {});
        foreignKeys[tableName][columnName] = true;
      }
    });
    return foreignKeys;
  }
);

const $tableUniqueColumns = createSelector(
  $uniqueColumnData,
  uniqueColumnData => {
    const tableUniqueColumns = {};
    uniqueColumnData.forEach(({ tableName, columnName }) => {
      ensureHashValue(tableUniqueColumns, tableName, {});
      tableUniqueColumns[tableName][columnName] = true;
    });
    return tableUniqueColumns;
  }
);

export const $tableColumns = createSelector(
  $tableColumnData,
  $tablePrimaryKeys,
  $tableForeignKeys,
  $tableUniqueColumns,
  (columns, tablePrimaryKeys, tableForeignKeys, tableUniqueColumns) => createGroupingDictionary(
    columns,
    c => c.tableName,
    c => ({
      ...c,
      unique: (c.tableName in tableUniqueColumns && c.columnName in tableUniqueColumns[c.tableName]),
      primaryKey: (c.tableName in tablePrimaryKeys && c.columnName in tablePrimaryKeys[c.tableName]),
      foreignKey: (c.tableName in tableForeignKeys && c.columnName in tableForeignKeys[c.tableName]),
    }),
    (c1, c2) => {
      const alphabetic = c1.columnName.localeCompare(c2.columnName);
      const c1isPrimary = c1.tableName in tablePrimaryKeys && c1.columnName in tablePrimaryKeys[c1.tableName];
      const c2isPrimary = c2.tableName in tablePrimaryKeys && c2.columnName in tablePrimaryKeys[c2.tableName];
      if (c1isPrimary && !c2isPrimary) {
        return -1;
      }
      if (!c1isPrimary && c2isPrimary) {
        return 1;
      }
      return alphabetic;
    },
  ),
);

export const $tableNames = createSelector(
  $tableColumns,
  tableColumns => Object.keys(tableColumns).sort(),
);

export const $relatedTables = createSelector(
  $tableNames,
  $foreignKeyData,
  (tableNames, foreignKeyData) => {
    const relatedTables = {};
    tableNames.forEach((tableName) => {
      ensureHashValue(relatedTables, tableName, {});
    });
    foreignKeyData.forEach(({ tableName, foreignTable }) => {
      relatedTables[tableName][foreignTable] = true;
      relatedTables[foreignTable][tableName] = true;
    });
    return relatedTables;
  }
);

export const $tableReferencedTables = createSelector(
  $tableNames,
  $foreignKeyData,
  (tableNames, foreignKeyData) => {
    const tableReferencedTables = {};
    tableNames.forEach((tableName) => {
      tableReferencedTables[tableName] = {};
    });
    foreignKeyData.forEach(({ tableName, foreignTable }) => {
      tableReferencedTables[tableName][foreignTable] = true;
    });
    return tableReferencedTables;
  }
);

export const $tableReferencingTables = createSelector(
  $tableNames,
  $foreignKeyData,
  (tableNames, foreignKeyData) => {
    const tableReferencingTables = {};
    tableNames.forEach((tableName) => {
      tableReferencingTables[tableName] = {};
    });
    foreignKeyData.forEach(({ tableName, foreignTable }) => {
      tableReferencingTables[foreignTable][tableName] = true;
    });
    return tableReferencingTables;
  }
);

export const $tableIndexes = createSelector(
  $indexData,
  indexData => createGroupingDictionary(indexData, index => index.tableName),
);
