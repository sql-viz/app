
import { remote } from 'electron';
import { $saveState, $filePath } from './selectors';
import { loadFile, storeFile } from '../../utils/files';

export const SAVE_DATABASE_SNAPSHOT_END = 'SAVE_END';
export const SAVE_DATABASE_SNAPSHOT_START = 'SAVE_START';
export const SET_FILE_PATH = 'SET_FILE_PATH';
export const LOAD_DATABASE_SNAPSHOT_START = 'LOAD_DATABASE_DATA_FROM_FILE_START';
export const LOAD_DATABASE_SNAPSHOT_END = 'LOAD_DATABASE_DATA_FROM_FILE_END';

export function saveDatabaseSnapshotAction() {
  return (dispatch, getState) => {
    dispatch({ type: SAVE_DATABASE_SNAPSHOT_START });
    const state = getState();
    const saveState = $saveState(state);
    const path = $filePath(state) || remote.dialog.showSaveDialog({ filters: [{ name: 'SQLViz snapshot', extensions: ['json.gz'] }] });
    if (path) {
      return storeFile(path, saveState).then(() => dispatch({ type: SAVE_DATABASE_SNAPSHOT_END, payload: path }));
    }
    dispatch({ type: SAVE_DATABASE_SNAPSHOT_END, payload: null });
  };
}

export function loadDatabaseSnapshotAction(path) {
  return dispatch => {
    dispatch({ type: LOAD_DATABASE_SNAPSHOT_START });
    return loadFile(path).then((data) => {
      dispatch({ type: SET_FILE_PATH, payload: path });
      return dispatch({ type: LOAD_DATABASE_SNAPSHOT_END, payload: data });
    });
  };
}
