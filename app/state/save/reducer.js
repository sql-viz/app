
import {
  SAVE_DATABASE_SNAPSHOT_END,
  SAVE_DATABASE_SNAPSHOT_START,
  SET_FILE_PATH,
  LOAD_DATABASE_SNAPSHOT_END,
  LOAD_DATABASE_SNAPSHOT_START,
} from './actions';

import {
  SELECT_TABLE,
  DESELECT_TABLE,
  SET_TABLE_SELECTION,
  OVERRIDE_TABLE_POSITIONS,
} from '../viewBuilder';

const initialState = {
  saving: false,
  loading: false,
  filePath: null,
  unsavedChanges: false,
};

export function saveReducer(state = initialState, { type, payload }) {
  switch (type) {
    case SAVE_DATABASE_SNAPSHOT_START: return { ...state, saving: true };
    case SAVE_DATABASE_SNAPSHOT_END: return { ...state, saving: false, unsavedChanges: false, filePath: payload };
    case LOAD_DATABASE_SNAPSHOT_START: return { ...state, loading: true };
    case LOAD_DATABASE_SNAPSHOT_END: return { ...state, loading: false };
    case SET_FILE_PATH: return { ...state, filePath: payload };
    case SELECT_TABLE: return { ...state, unsavedChanges: true };
    case DESELECT_TABLE: return { ...state, unsavedChanges: true };
    case SET_TABLE_SELECTION: return { ...state, unsavedChanges: true };
    case OVERRIDE_TABLE_POSITIONS: return { ...state, unsavedChanges: true };
    default: return state;
  }
}
