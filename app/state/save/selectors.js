
import { createSelector } from 'reselect';
import { $database } from '../database';
import { $viewBuilder } from '../viewBuilder';

const $save = state => state.save;

export const $filePath = createSelector(
  $save,
  save => save.filePath,
);

export const $loadingSnapshot = createSelector(
  $save,
  save => save.loading,
);

export const $savingSnapshot = createSelector(
  $save,
  save => save.saving,
);

export const $saveState = createSelector(
  $database,
  $viewBuilder,
  (database, viewBuilder) => ({ database, viewBuilder }),
);

export const $unsavedChanges = createSelector(
  $save,
  save => save.unsavedChanges,
);

export const $hasUnsavedChanges = createSelector(
  $filePath,
  $unsavedChanges,
  (filePath, unsavedChanges) => !filePath || unsavedChanges,
);
