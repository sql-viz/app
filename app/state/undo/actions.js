
import { $undoHistoryEmpty, $undoHistoryPreviousAction } from './selectors';

export const PUSH_ACTION_TO_HISTORY = 'PUSH_ACTION_TO_HISTORY';
export const UNDO_ACTION_FROM_HISTORY = 'UNDO_ACTION_FROM_HISTORY';

export function pushActionToHistoryAction(action) {
  return { type: PUSH_ACTION_TO_HISTORY, payload: action };
}

export function undoActionFromHistoryAction() {
  return (dispatch, getState) => {
    const state = getState();
    if (!$undoHistoryEmpty(state)) {
      const action = $undoHistoryPreviousAction(state);
      dispatch({ type: UNDO_ACTION_FROM_HISTORY });
      dispatch(action);
    }
  };
}
