
import { combineReducers } from 'redux';

import { PUSH_ACTION_TO_HISTORY, UNDO_ACTION_FROM_HISTORY } from './actions';

function historyReducer(state = [], { type, payload }) {
  switch (type) {
    case PUSH_ACTION_TO_HISTORY: return [...state, payload];
    case UNDO_ACTION_FROM_HISTORY: return state.splice(0, state.length - 1);
    default: return state;
  }
}

export const undoReducer = combineReducers({
  history: historyReducer,
});
