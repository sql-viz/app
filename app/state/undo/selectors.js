
import { createSelector } from 'reselect';

const $undo = state => state.undo;

const $undoHistory = createSelector($undo, undo => undo.history);

export const $undoHistoryEmpty = createSelector($undoHistory, history => history.length === 0);

export const $undoHistoryPreviousAction = createSelector($undoHistory, histoy => histoy[histoy.length - 1]);
