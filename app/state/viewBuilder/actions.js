
import { createDictionary } from '../../utils';

import { pushActionToHistoryAction } from '../undo';

import { $tableNames } from '../database';
import { $selectedTables, $tablePositions } from './selectors';

export const SELECT_TABLE = 'SELECT_TABLE';
export const DESELECT_TABLE = 'DESELECT_TABLE';
export const SET_TABLE_SELECTION = 'SET_TABLE_SELECTION';
export const PIN_TABLE = 'PIN_TABLE';
export const UNPIN_TABLE = 'UNPIN_TABLE';
export const OVERRIDE_TABLE_POSITIONS = 'OVERRIDE_TABLE_POSITIONS';
export const OVERRIDE_VIEWPORT = 'OVERRIDE_VIEWPORT';

export function selectTableAction(table) {
  return (dispatch, getState) => {
    const tableSelection = $selectedTables(getState());
    dispatch({ type: SELECT_TABLE, payload: table });
    dispatch(pushActionToHistoryAction({ type: SET_TABLE_SELECTION, payload: tableSelection }));
  };
}

export function deselectTableAction(table) {
  return (dispatch, getState) => {
    const tableSelection = $selectedTables(getState());
    dispatch({ type: DESELECT_TABLE, payload: table });
    dispatch(pushActionToHistoryAction({ type: SET_TABLE_SELECTION, payload: tableSelection }));
  };
}

export function deselectAllTablesAction() {
  return (dispatch, getState) => {
    const state = getState();
    const tableSelection = $selectedTables(state);
    const selectedTables = Object.keys(tableSelection);
    if (selectedTables.length > 0) {
      dispatch({
        type: SET_TABLE_SELECTION,
        payload: {},
      });
      dispatch(pushActionToHistoryAction({ type: SET_TABLE_SELECTION, payload: tableSelection }));
    }
  };
}

export function selectAllTablesAction() {
  return (dispatch, getState) => {
    const state = getState();
    const allTables = $tableNames(state);
    const tableSelection = $selectedTables(state);
    const selectedTables = Object.keys(tableSelection);
    if (selectedTables.length !== allTables.length) {
      dispatch({
        type: SET_TABLE_SELECTION,
        payload: createDictionary(allTables, table => table, () => true),
      });
      dispatch(pushActionToHistoryAction({ type: SET_TABLE_SELECTION, payload: tableSelection }));
    }
  };
}

export function invertTableSelectionAction() {
  return (dispatch, getState) => {
    const state = getState();
    const tableSelection = $selectedTables(state);
    const allTables = $tableNames(state);
    dispatch({
      type: SET_TABLE_SELECTION,
      payload: createDictionary(allTables.filter(table => !(table in tableSelection)), table => table, () => true),
    });
    dispatch(pushActionToHistoryAction({ type: SET_TABLE_SELECTION, payload: tableSelection }));
  };
}

export function pinTableAction(table) {
  return { type: PIN_TABLE, payload: table };
}

export function unpinTableAction(table) {
  return { type: UNPIN_TABLE, payload: table };
}

export function overrideTablePositionsAction(positions) {
  return (dispatch, getState) => {
    const oldPositions = $tablePositions(getState());
    dispatch(pushActionToHistoryAction({ type: OVERRIDE_TABLE_POSITIONS, payload: { ...oldPositions } }));
    dispatch({ type: OVERRIDE_TABLE_POSITIONS, payload: { ...positions } });
  };
}

export function overrideViewportAction(viewport) {
  return { type: OVERRIDE_VIEWPORT, payload: viewport };
}
