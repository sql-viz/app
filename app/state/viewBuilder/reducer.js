
import { combineReducers } from 'redux';
import { DESELECT_TABLE, SELECT_TABLE, SET_TABLE_SELECTION, PIN_TABLE, UNPIN_TABLE, OVERRIDE_TABLE_POSITIONS, UNDO_TABLE_POSITIONS, OVERRIDE_VIEWPORT } from './actions';
import { LOAD_DATABASE_SNAPSHOT_END } from '../save';

function selectedTableReducer(state = {}, { type, payload }) {
  switch (type) {
    case SELECT_TABLE: return { ...state, [payload]: true };
    case DESELECT_TABLE: {
      const newState = { ...state };
      delete newState[payload];
      return newState;
    }
    case SET_TABLE_SELECTION: return payload;
    case LOAD_DATABASE_SNAPSHOT_END: return { ...state, ...payload.viewBuilder.selectedTables };
    default: return state;
  }
}

function pinnedTableReducer(state = null, { type, payload }) {
  switch (type) {
    case PIN_TABLE: return payload;
    case UNPIN_TABLE: return null;
    case LOAD_DATABASE_SNAPSHOT_END: return payload.viewBuilder.pinnedTable;
    default: return state;
  }
}

function tablePositionsReducer(state = null, { type, payload }) {
  switch (type) {
    case OVERRIDE_TABLE_POSITIONS: return { ...payload };
    case LOAD_DATABASE_SNAPSHOT_END: return payload.viewBuilder.tablePositions || null;
    default: return state;
  }
}

function viewportReducer(state = null, { type, payload }) {
  switch (type) {
    case OVERRIDE_VIEWPORT: return payload;
    case LOAD_DATABASE_SNAPSHOT_END: return payload.viewBuilder.viewport || null;
    default: return state;
  }
}

export const viewBuilderReducer = combineReducers({
  selectedTables: selectedTableReducer,
  pinnedTable: pinnedTableReducer,
  tablePositions: tablePositionsReducer,
  viewport: viewportReducer,
});
