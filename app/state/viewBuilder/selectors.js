
import { createSelector } from 'reselect';
import { $tableColumns, $foreignKeyData } from '../database';
import { ensureHashValue } from '../../utils';
import { Digraph } from '../../models';

export const $viewBuilder = state => state.viewBuilder;

export const $selectedTables = createSelector(
  $viewBuilder,
  viewBuilder => viewBuilder.selectedTables,
);

export const $tablePositions = createSelector(
  $viewBuilder,
  viewBuilder => viewBuilder.tablePositions,
);

export const $viewport = createSelector(
  $viewBuilder,
  viewBuilder => viewBuilder.viewport,
);

const $selectedTablesForeignKeyData = createSelector(
  $selectedTables,
  $foreignKeyData,
  (selectedTables, foreignKeyData) => foreignKeyData.filter(({ tableName, foreignTable }) => (tableName in selectedTables) && (foreignTable in selectedTables))
);

export const $selectedTableColumnNames = createSelector(
  $selectedTables,
  $tableColumns,
  (selectedTables, tableColumns) => {
    const columnNames = {};
    Object.keys(selectedTables).forEach((table) => {
      columnNames[table] = tableColumns[table].map(column => column.columnName);
    });
    return columnNames;
  }
);

export const $selectedTableKeyLinks = createSelector(
  $selectedTablesForeignKeyData,
  (foreignKeyData) => {
    const links = {};
    foreignKeyData.forEach(({ tableName, columnName, foreignTable, foreignColumn }) => {
      ensureHashValue(links, tableName, {});
      ensureHashValue(links[tableName], foreignTable, []);
      links[tableName][foreignTable].push([columnName, foreignColumn]);
    });
    return links;
  }
);

export const $selectedTableRelationDigraph = createSelector(
  $selectedTables,
  $selectedTablesForeignKeyData,
  (selectedTables, foreignKeyData) => {
    const nodeHash = {};
    Object.keys(selectedTables).forEach((table) => {
      nodeHash[table] = { id: table, neighbors: [] };
    });
    foreignKeyData.forEach(({ tableName, foreignTable }) => {
      const id = tableName;
      const neighbor = foreignTable;
      if (id !== neighbor) {
        if (!nodeHash[neighbor].neighbors.includes(id)) {
          nodeHash[id].neighbors.push(neighbor);
        } else {
          console.log('loop', id, neighbor);
        }
      } else {
        console.log('self-referencing', id);
      }
    });
    return new Digraph(Object.values(nodeHash));
  }
);

export const $pinnedTable = createSelector(
  $viewBuilder,
  viewBuilder => viewBuilder.pinnedTable,
);
