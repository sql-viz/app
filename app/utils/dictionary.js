
export function createDictionary(array = [], keyFn = i => i, valueFn = i => i) {
  return array.reduce((dict, item) => {
    dict[keyFn(item)] = valueFn(item); // eslint-disable-line no-param-reassign
    return dict;
  }, {});
}

export function createGroupingDictionary(array = [], keyFn = i => i, valueFn = i => i, sort) {
  const grouped = array.reduce((dict, item) => {
    const key = keyFn(item);
    if (!(key in dict)) {
      dict[key] = [];
    }
    dict[key].push(valueFn(item));
    return dict;
  }, {});
  if (typeof sort === 'function') {
    Object.keys(grouped).forEach((key) => {
      grouped[key].sort(sort);
    });
  }
  return grouped;
}
