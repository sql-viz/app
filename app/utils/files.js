
import zlib from 'zlib';
import fs from 'fs';

export function storeFile(path, data) {
  return new Promise((resolve, reject) => {
    zlib.deflate(JSON.stringify(data), (compressionError, buffer) => {
      if (compressionError) {
        reject(compressionError);
      } else {
        fs.writeFile(path, buffer.toString('base64'), (saveError) => {
          if (saveError) {
            reject(saveError);
          } else {
            resolve(path);
          }
        });
      }
    });
  });
}

export function loadFile(path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, { encoding: 'utf8' }, (readError, contents) => {
      if (readError) {
        reject(readError);
      } else {
        zlib.unzip(Buffer.from(contents, 'base64'), (unzipError, buffer) => {
          if (unzipError) {
            reject(unzipError);
          } else {
            resolve(JSON.parse(buffer.toString()));
          }
        });
      }
    });
  });
}
