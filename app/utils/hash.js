
export function ensureHashValue(hash, key, value) {
  if (!(key in hash)) {
    hash[key] = value; //  eslint-disable-line no-param-reassign
  }
}
