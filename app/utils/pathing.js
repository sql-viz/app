
function pointDistance(p1, p2) {
  const [x1, y1, x2, y2] = [...p1, ...p2];
  return Math.sqrt(((x2 - x1) ** 2) + ((y2 - y1) ** 2));
}

export function calculatePathLength(path) {
  const [start, ...rest] = path;
  let current = start;
  let sum = 0;
  while (rest.length > 0) {
    const next = rest.shift(0);
    sum += pointDistance(current, next);
    current = next;
  }
  return sum;
}
